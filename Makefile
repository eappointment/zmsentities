COMPOSER=php -d suhosin.executor.include.whitelist=phar bin/composer.phar

now: # Dummy target

dev: # init development system
	$(COMPOSER) update

live: # init live system, delete unnecessary libs
	$(COMPOSER) install --no-dev --prefer-dist

tests: now # run tests
	../../bin/phpmd src/ text phpmd.rules.xml
	../../bin/phpcs --standard=psr2 src/
	php ../../bin/phpunit --coverage-html public/_tests/coverage/

paratest: # init parallel unit testing with 5 processes
	vendor/bin/paratest -c paratest.xml --coverage-html public/_tests/coverage/

fix: #f fix code
	php ../../bin/phpcbf --standard=psr2 src/

coverage:
	 XDEBUG_MODE=coverage php vendor/bin/phpunit --coverage-html public/_tests/coverage/
