<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests;

use BO\Zmsentities\EventLog;

class EventLogTest extends EntityCommonTests
{
    public $entityclass = '\BO\Zmsentities\EventLog';

    public function testSetSecondsToLive(): void
    {
        $entity = new EventLog();
        $entity->setSecondsToLive(EventLog::LIVETIME_DEFAULT);

        self::assertInstanceOf('\DateTimeInterface', $entity->expirationDateTime);

        $json = '{';
        $json .= '"$schema":"https:\\/\\/schema.berlin.de\\/queuemanagement\\/eventlog.json",';
        $json .= '"id":0,"name":"","origin":"","referenceType":"none",';
        $json .=  '"creationDateTime":"' . $entity['creationDateTime']->format(DATE_ATOM) . '",';
        $json .=  '"expirationDateTime":"' . $entity['expirationDateTime']->format(DATE_ATOM) . '"';
        $json .= '}';

        self::assertSame($json, $entity->__toString());
    }
}
