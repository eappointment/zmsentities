<?php

namespace BO\Zmsentities\Tests;

use BO\Zmsentities\ProcessArchive;
use Ramsey\Uuid\Uuid;

class ProcessArchiveTest extends EntityCommonTests
{
    public $entityclass = '\BO\Zmsentities\ProcessArchive';

    public $collectionclass = '\BO\Zmsentities\Collection\ProcessArchiveList';

    public function testBasic()
    {
        /** @var ProcessArchive $entity */
        $entity = $this->getExample();
        $uuidString = $entity::getUuid();

        self::assertEquals(32, strlen($uuidString));
        self::assertStringEndsWith(ucfirst($entity->getEntityName()), get_class($entity));
        self::assertSame('processArchive#550e8400e29b41d4a716446655440000 (reserved) ~rjuyy6', $entity->__toString());
    }
}
