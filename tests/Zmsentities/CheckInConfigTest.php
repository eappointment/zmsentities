<?php

declare(strict_types=1);

namespace BO\Zmsentities\Tests;

use BO\Zmsentities\CheckInConfig;

class CheckInConfigTest extends Base
{
    public $entityclass = '\BO\Zmsentities\CheckInConfig';

    public function testExample(): void
    {
        $sut = CheckInConfig::getExample();

        self::assertTrue($sut->isValid());
        self::assertTrue($sut->isActive());
        self::assertTrue($sut->isOperating());

        $translations = $sut->getTranslations('en_GB');
        self::assertCount(7, $translations);

        $translations = $sut->getTranslations('no_NO');
        self::assertCount(0, $translations);

        $expected = '{'
         . '"$schema":"https:\/\/schema.berlin.de\/queuemanagement\/checkInConfig.json",'
         . '"id":"25f9e794323b",'
         . '"status":"on","scopeIds":":123:456:",'
         . '"startBeforeAvailability":false,"startBeforeMinutes":15,'
         . '"tooEarlyMinutes":30,"toleranceMinutes":0,'
         . '"activeAfterClosing":false,'
         . '"languages":"de_DE,en_GB",'
         . '"msgAlternative":"{\u0022en_GB\u0022:\u0022Please proceed to the reception.\u0022}",'
         . '"msgLate":"{\u0022en_GB\u0022:\u0022You are late.\u0022}",'
         . '"msgEarly":"{\u0022en_GB\u0022:\u0022You are early.\u0022}",'
         . '"msgWrongDay":"{\u0022en_GB\u0022:\u0022You came the wrong day.\u0022}",'
         . '"msgWrongCounter":"{\u0022en_GB\u0022:\u0022You have mistaken the place where to show up.\u0022}",'
         . '"msgSuccess":"{\u0022en_GB\u0022:\u0022Please take a seat.\u0022}",'
         . '"msgMaintenance":"{\u0022en_GB\u0022:\u0022Please proceed to the reception.\u0022}",'
         . '"lastChange":"2015-11-03 10:33:30"'
         . '}';

        self::assertSame($expected, $sut->__toString());
    }

    public function testGetScopeIds(): void
    {
        $sut = new CheckInConfig(['scopeIds' => ':123:456:789:']);
        self::assertSame([123,456,789], $sut->getScopeIds());

        $sut->setScopeIds([333,444]);
        self::assertSame(':333:444:', $sut['scopeIds']);

        $sut = CheckInConfig::getExample();
        self::assertIsString($sut->getId());
        self::assertSame([123,456], $sut->getScopeIds());
    }

    public function testIsScopeContained(): void
    {
        $sut = new CheckInConfig(['scopeIds' => ':123:456:789:']);

        self::assertTrue($sut->isScopeContained(456));
        self::assertFalse($sut->isScopeContained(321));
    }
}
