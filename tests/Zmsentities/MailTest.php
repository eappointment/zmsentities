<?php

namespace BO\Zmsentities\Tests;

use BO\Mellon\Validator;
use BO\Zmsentities\Client;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Config;
use BO\Zmsentities\Department;
use BO\Zmsentities\Helper\Messaging;
use BO\Zmsentities\Mail;
use BO\Zmsentities\Mail as MailEntity;
use BO\Zmsentities\Mimepart;
use BO\Zmsentities\Process;
use BO\Zmsentities\Provider;
use BO\Zmsentities\Request;
use BO\Zmsentities\Scope;

/**
 * @method Mail getExample()
 */
class MailTest extends EntityCommonTests
{
    public $entityclass = '\BO\Zmsentities\Mail';

    public $collectionclass = '\BO\Zmsentities\Collection\MailList';

    const DEFAULT_TIME = '2016-04-01 11:55:00';

    public function testBasic()
    {
        $entity = $this->getExample();
        $this->assertTrue(123456 == $entity->getProcessId(), 'Getting process id failed');
        $this->assertTrue('1234' == $entity->getProcessAuthKey(), 'Getting AuthKey failed');
        $this->assertTrue('Moritz Mustermann' == $entity->getClient()['familyName'], 'Getting client failed');
        $this->assertTrue('Max Mustermann' == $entity->getFirstClient()['familyName'], 'Getting first client failed');
        $this->assertTrue($entity->hasContent(), 'Missing content in mail');

        $expectedString = 'mail#1234 recipient:max@service.berlin.de process:123456';

        self::assertSame($expectedString, $entity->__toString());
    }

    public function testDateformat()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $dateTime = new \DateTimeImmutable("2019-12-30 11:55:00", new \DateTimeZone('Europe/Berlin'));
        $process->getFirstAppointment()->setDateTime($dateTime);
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'appointment');
        $this->assertStringContainsString(
            'Montag, 30. Dezember 2019 um 11:55 Uhr',
            $resolvedEntity->getHtmlPart(),
            'Wrong date/time format'
        );
    }


    public function testCollection()
    {
        $collection = new $this->collectionclass();
        $entity = $this->getExample();
        $collection->addEntity($entity);
        $this->assertEntityList($this->entityclass, $collection);
        $this->assertTrue($collection->hasEntity(1234), "Missing Test Entity with ID 1234 in collection");
        $filterProcess = $collection->withProcess('123456');
        $this->assertCount(1, $filterProcess);
    }

    public function testMultiPart()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->requests[] = (new Request())->getExample();
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $this->assertTrue(null === $entity->getHtmlPart(), 'Mimepart with mime text/html should not exist');
        $this->assertTrue(null === $entity->getPlainPart(), 'Mimepart with mime text/plain should not exist');
        $this->assertFalse($entity->hasIcs(), 'Mimepart with mime text/calendar should not exist');
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'appointment');
        $this->assertStringContainsString(
            'Guten Tag',
            $resolvedEntity->getHtmlPart(),
            'Mimepart content is not html'
        );
        $this->assertStringContainsString(
            '**Vorgangsnummer:** 123456',
            $resolvedEntity->getPlainPart(),
            'Mimepart content is not plain text'
        );
        $this->assertStringContainsString('BEGIN:VCALENDAR', $resolvedEntity->getIcsPart(), 'Mimepart content is not plain text');
        // test if appointment date formatted correct
        $this->assertStringContainsString('18. November 2015 um 18:52', $resolvedEntity->getIcsPart());
        $this->assertStringContainsString('DTSTART;TZID=Europe/Berlin:20151118T185251', $resolvedEntity->getIcsPart());
    }

    public function testICS()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->requests[] = (new Request())->getExample();
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'appointment');

        $this->assertStringContainsString('Achtung! Dies ist eine automatisch erstellte E-Mail', $resolvedEntity->getIcsPart(), 'ICS content is not valid');
        $this->assertStringContainsString('Guten Tag', $resolvedEntity->getIcsPart(), 'ICS content is not valid');
        // test if appointment date formatted correct
    }

    public function testXlsx()
    {
        $entity = $this->getExample();
        $entity->process = new Process(['id' => 999999]); //only as default because process is required
        $entity->process->getFirstClient()->email = 'max.mustermann@example.com';
        $entity->department = (new Department())->getExample();
        $entity->createIP = '0.0.0.0';

        $multipart[] = new Mimepart([
            'mime' => Mimepart::MIME_TYPE_XLSX,
            'content' => 'dW5pdHRlc3Q=',
            'base64' => true,
            'attachmentName' => 'attachment.xlsx',
        ]);
        $multipart[] = new Mimepart([
            'mime' => Mimepart::MIME_TYPE_HTML,
            'content' => 'unittest',
            'base64' => false
        ]);
        $multipart[] = new Mimepart([
            'mime' => Mimepart::MIME_TYPE_TEXT,
            'content' => Messaging::getPlainText('unittest'),
            'base64' => false
        ]);

        $entity->addMultiPart($multipart);
        $entity->client = null;

        $this->assertStringContainsString('unittest', $entity->getXlsxPart(), 'xlsx content is not valid');
    }

    public function testConfirmationWithPaymentMethods()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->scope = (new Scope())->getExample();
        $process->scope->provider = (new Provider())->getExample();
        $process->scope->provider['data']['payment_methods'] = [
            [
                "code" => 1,
                "text" => "Es kann Bar bezahlt werden.",
                "icon" => null
            ],
            [
                "code" => 2,
                "text" => "Es kann mit girocard (mit PIN) bezahlt werden.",
                "icon" => null
            ]
        ];
        $process->scope->provider['data']['payment'] = "Es kann Bar bezahlt werden.\nEs kann mit girocard (mit PIN) bezahlt werden.";
        $process->requests->getFirst()->data = null;
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'appointment');
        $this->assertStringContainsString('Guten Tag', $resolvedEntity->getHtmlPart());
        $this->assertStringNotContainsString('Erforderliche Unterlagen', $resolvedEntity->getHtmlPart());
        $this->assertStringContainsString('BEGIN:VCALENDAR', $resolvedEntity->getIcsPart());
        $this->assertStringContainsString('Zahlungshinweis', $resolvedEntity->getHtmlPart());
        $this->assertStringContainsString('Es kann Bar bezahlt werden.', $resolvedEntity->getHtmlPart());
        $this->assertStringContainsString(
            'Es kann mit girocard (mit PIN) bezahlt werden.',
            $resolvedEntity->getHtmlPart()
        );
    }

    public function testConfirmationWithOldPayment()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->scope = (new Scope())->getExample();
        $process->scope->provider = (new Provider())->getExample();
        $process->scope->provider['data']['payment'] = "Es kann Bar bezahlt werden.\nEs kann mit girocard (mit PIN) bezahlt werden.";
        $process->requests->getFirst()->data = null;
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'appointment');
        $this->assertStringContainsString('Guten Tag', $resolvedEntity->getHtmlPart());
        $this->assertStringNotContainsString('Erforderliche Unterlagen', $resolvedEntity->getHtmlPart());
        $this->assertStringContainsString('BEGIN:VCALENDAR', $resolvedEntity->getIcsPart());
        $this->assertStringContainsString('Zahlungshinweis', $resolvedEntity->getHtmlPart());
        $this->assertStringContainsString('Es kann Bar bezahlt werden.', $resolvedEntity->getHtmlPart());
        $this->assertStringContainsString(
            'Es kann mit girocard (mit PIN) bezahlt werden.',
            $resolvedEntity->getHtmlPart()
        );
    }

    public function testMailWithInitiator()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'updated', 'admin');
        $this->assertStringContainsString(
            'Geändert wurde der Termin von Max Mustermann',
            $resolvedEntity->getHtmlPart(),
            'Mimepart content is not html'
        );
        $this->assertStringContainsString(
            'Die Terminänderung wurde initiiert via "admin"',
            $resolvedEntity->getPlainPart(),
            'Mimepart content is not plain text'
        );
    }

    public function testMailWithSurveyAccepted()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->scope['preferences']['survey']['emailContent'] = 'Das ist eine Umfrage';
        $process->getFirstClient()->surveyAccepted = 1;
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'survey');
        $this->assertStringContainsString(
            'Das ist eine Umfrage',
            $resolvedEntity->getPlainPart(),
            'Mimepart content is not plain text'
        );
    }

    public function testQueuedMailWithOneRequest()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->scope = (new Scope())->getExample();
        $process->scope->provider = (new Provider())->getExample();
        $process->scope->provider['data']['payment_methods'] = [
            [
                "code" => 1,
                "text" => "Es kann Bar bezahlt werden.",
                "icon" => null
            ],
            [
                "code" => 2,
                "text" => "Es kann mit girocard (mit PIN) bezahlt werden.",
                "icon" => null
            ]
        ];
        $process->scope->provider['data']['payment'] = "Es kann Bar bezahlt werden.\nEs kann mit girocard (mit PIN) bezahlt werden.";
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'queued');
        $this->assertStringContainsString('Sie haben folgende Dienstleistung ausgewählt:', $resolvedEntity->getPlainPart());
        $this->assertStringContainsString('Zahlungshinweis', $resolvedEntity->getHtmlPart());
        $this->assertStringContainsString('Es kann Bar bezahlt werden.', $resolvedEntity->getHtmlPart());
        $this->assertStringContainsString(
            'Es kann mit girocard (mit PIN) bezahlt werden.',
            $resolvedEntity->getHtmlPart()
        );
    }

    public function testQueuedMailWithMultipleRequests()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->requests[] = (new Request())->getExample();
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'queued');
        $this->assertStringContainsString('Sie haben folgende Dienstleistungen ausgewählt:', $resolvedEntity->getPlainPart());
    }

    public function testQueuedMailWithoutRequests()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->requests = [];
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'queued');
        $this->assertStringContainsString('Sie haben keine Dienstleistungen ausgewählt.', $resolvedEntity->getPlainPart());
    }

    public function testMailWithOneRequest()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'appointment');
        $this->assertStringContainsString('Sie haben folgende Dienstleistung ausgewählt:', $resolvedEntity->getPlainPart());
    }

    public function testMailWithMultipleRequests()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->requests[] = (new Request())->getExample();
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'appointment');
        $this->assertStringContainsString('Sie haben folgende Dienstleistungen ausgewählt:', $resolvedEntity->getPlainPart());
    }

    public function testMailWithoutRequests()
    {
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->requests = [];
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'appointment');
        $this->assertStringContainsString('Sie haben keine Dienstleistungen ausgewählt.', $resolvedEntity->getPlainPart());
    }

    public function testTemplateNotFound()
    {
        $this->expectException('\BO\Zmsentities\Exception\TemplateNotFound');
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->getFirstClient()->surveyAccepted = 0;
        $config = (new Config())->getExample();
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'finished');
    }

    public function testToCustomMessageEntity()
    {
        $entity = (new $this->entityclass());
        $process = (new Process())->getExample();
        $config = (new Config())->getExample();
        $formCollection = array(
            'subject' => Validator::value('Das ist ein Test')->isString()->isBiggerThan(2),
            'message' => Validator::value('Das ist eine Testnachricht')->isString()->isBiggerThan(2)
        );
        $formCollection = Validator::collection($formCollection);
        $customEntity = $entity->toCustomMessageEntity($process, $config, $formCollection->getValues());
        $this->assertEquals('Das ist ein Test', $customEntity->subject);
        $this->assertStringContainsString('Guten Tag Max Mustermann,', $customEntity->getPlainPart());
        $this->assertStringContainsString('Das ist eine Testnachricht', $customEntity->getPlainPart());
        $this->assertStringContainsString('Ihre Terminübersicht', $customEntity->getPlainPart());
        $this->assertFalse($customEntity->hasIcs());
    }

    public function testToCustomMessageEntityWithoutClient()
    {
        $entity = (new $this->entityclass());
        $process = (new Process())->getExample();
        $config = (new Config())->getExample();
        $process->addClientFromForm(['email' => 'unittest@berlinonline.de']);
        $formCollection = array(
            'subject' => Validator::value('Das ist ein Test')->isString()->isBiggerThan(2),
            'message' => Validator::value('Das ist eine Testnachricht')->isString()->isBiggerThan(2)
        );
        $formCollection = Validator::collection($formCollection);
        $entity->client = null;
        $customEntity = $entity->toCustomMessageEntity($process, $config, $formCollection->getValues());
        $this->assertEquals('unittest@berlinonline.de', $customEntity['client']['email']);
        $this->assertEquals('Das ist ein Test', $customEntity->subject);
        $this->assertStringContainsString('Guten Tag,', $customEntity->getPlainPart());
        $this->assertStringContainsString('Das ist eine Testnachricht', $customEntity->getPlainPart());
        $this->assertStringContainsString('Ihre Terminübersicht', $customEntity->getPlainPart());
        $this->assertFalse($customEntity->hasIcs());
    }

    public function testToScopeAdminProcessList()
    {
        $now = new \DateTimeImmutable(self::DEFAULT_TIME);
        $process = (new Process())->getExample();
        $processList = (new ProcessList())->addEntity($process);
        $scope = (new Scope())->getExample();
        $entity = (new $this->entityclass())->toScopeAdminProcessList($processList, $scope, $now);
        $this->assertEquals('Termine am 2016-04-01', $entity->subject);
        $this->assertStringContainsString('Termine am 2016-04-01 (1 gesamt)', $entity->getHtmlPart());
        $this->assertStringContainsString('18:52 <small>(2)</small>', $entity->getHtmlPart());
        $this->assertEquals(null, $entity->getPlainPart());
        $this->assertEquals(null, $entity->getIcsPart());
    }

    public function testToOptinEntity()
    {
        $now = new \DateTimeImmutable(self::DEFAULT_TIME);
        $process = (new Process())->getExample();
        $config = (new Config())->getExample();
        $client =  $process->getFirstClient();
        $code = substr(hash('sha256', $process->getId().$client->email),0,6);
        $entity = (new $this->entityclass())->toOptInEntity($process, $config, $code);
        $this->assertEquals('Bitte verifizieren Sie Ihre E-Mail-Adresse', $entity->subject);
        $this->assertStringContainsString('vielen Dank für Ihre Terminanforderung', $entity->getHtmlPart());
        $this->assertStringContainsString(
            '<h2>d379f5</h2>', $entity->getHtmlPart()
        );
        $this->assertEquals(null, $entity->getIcsPart());
    }

    public function testWithDepartment()
    {
        $entity = $this->getExample();
        $department = (new Department)->getExample();
        $entity->withDepartment($department);
        $this->assertEquals('Flughafen Schönefeld, Landebahn', $entity->department->getContact()->name);
    }

    public function testGetRecipient()
    {
        $entity = $this->getExample();
        $this->assertEquals('max@service.berlin.de', $entity->getRecipient());
    }

    public function testGetRecipientFailed()
    {
        $this->expectException('BO\Zmsentities\Exception\MailMissedAddress');
        $entity = $this->getExample();
        unset($entity->process);
        unset($entity->client);
        $entity->getRecipient();
    }

    public function testWithProcessClient(): void
    {
        $entity = new Mail([]);
        $entity->client = null; // reason for result
        $entity->process = new Process(['clients' => [new Client(['email' => 'this@example.com'])]]);

        self::assertSame('this@example.com', $entity->getFirstClient()->email);
        self::assertTrue($entity->getClient()->hasEmail());

        $entity->withProcessClient('another@exmple.com');

        self::assertSame('this@example.com', $entity->getClient()->email); // @todo: is this expected?

        $entity = new Mail([]);
        $entity->process = new Process(['clients' => [new Client(['email' => 'this@example.com'])]]);
        $entity->withProcessClient('another@example.com');

        self::assertSame('another@example.com', $entity->getClient()->email);
    }

    public function testMailTemplatesAll()
    {
        $statusList = array(
            'queued',
            'appointment',
            'reserved',
            'reminder',
            'pickup',
            'deleted',
            'blocked',
            'survey'
        );
        $statusAdminList = array(
            'deleted',
            'blocked',
            'updated'
        );
        $statusFailedList = array(
            "free",
            "reserved",
            "called",
            "processing",
            "pending",
            "finished",
            "missed",
            "archived",
            "anonymized",
            "conflict",
            ""
        );
        $entity = $this->getExample();
        $process = (new Process())->getExample();
        $process->scope = (new Scope())->getExample();
        $process->queue->withAppointment = false;

        //survey mail with and without clientname
        $config = (new Config())->getExample();
        $entity->addMultiPart(array());
        $entity->client = null;
        $resolvedEntity = $entity->toResolvedEntity($process, $config, 'survey');
        $this->assertStringContainsString('Guten Tag Max Mustermann', $resolvedEntity->getPlainPart());
        $processSurvey = clone $process;
        $processSurvey->getFirstClient()->familyName = null;
        $resolvedEntity = $entity->toResolvedEntity($processSurvey, $config, 'survey');
        $this->assertStringContainsString('Guten Tag', $resolvedEntity->getPlainPart());
        
        //all others by status
        foreach ($statusList as $status) {
            $config = (new Config())->getExample();
            $entity->addMultiPart(array());
            $entity->client = null;
            $resolvedEntity = $entity->toResolvedEntity($process, $config, $status);
            $this->assertStringContainsString('Guten Tag', $resolvedEntity->getPlainPart());
            $this->assertStringContainsString(
                'Achtung! Dies ist eine automatisch erstellte E-Mail.',
                $resolvedEntity->getPlainPart()
            );
        }

        foreach ($statusAdminList as $status) {
            $config = (new Config())->getExample();
            $entity->addMultiPart(array());
            $entity->client = null;
            $resolvedEntity = $entity->toResolvedEntity($process, $config, $status, 'unittest');
            $this->assertStringContainsString('initiiert via "unittest"', $resolvedEntity->getPlainPart());
        }

        foreach ($statusFailedList as $status) {
            $this->expectException('BO\Zmsentities\Exception\TemplateNotFound');
            $config = (new Config())->getExample();
            $entity->addMultiPart(array());
            $entity->client = null;
            $resolvedEntity = $entity->toResolvedEntity($process, $config, $status);
        }
    }
}
