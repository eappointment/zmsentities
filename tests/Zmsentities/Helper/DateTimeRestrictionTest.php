<?php

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Helper;

use BO\Zmsentities\Helper\TimeRestriction;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Process;
use PHPUnit\Framework\TestCase;

class DateTimeRestrictionTest extends TestCase
{
    const DEFAULT_TIME = '2016-04-01 11:55:00';

    const TIME_RESTRICTION_STRING = 'Mo-13-19,Mi-13-19,Fr-13-19';

    public function testBasic()
    {
        $restrictionHelper = new TimeRestriction('');
        self::assertInstanceOf(TimeRestriction::class, $restrictionHelper);
        self::assertFalse($restrictionHelper->isActive());
        self::assertEquals(28, count($restrictionHelper->getRestrictionParts()));

        $restrictionHelper = new TimeRestriction(self::TIME_RESTRICTION_STRING);
        self::assertTrue($restrictionHelper->isActive());
        self::assertEquals(3, count($restrictionHelper->getRestrictionParts()));
        self::assertEquals('Mo', $restrictionHelper->getRestrictionParts()['0']['day']);
        self::assertEquals('13', $restrictionHelper->getRestrictionParts()['0']['start']);
        self::assertEquals('19', $restrictionHelper->getRestrictionParts()['0']['end']);

        $weekdays = $restrictionHelper->getDaysOfWeek();
        self::assertEquals('Montag', $weekdays['Mo']);
        self::assertEquals('Dienstag', $weekdays['Di']);
        self::assertEquals('Mittwoch', $weekdays['Mi']);
        self::assertEquals('Donnerstag', $weekdays['Do']);
        self::assertEquals('Freitag', $weekdays['Fr']);
        self::assertEquals('Samstag', $weekdays['Sa']);
        self::assertFalse(isset($weekdays['So']));

        $activeDayParts = $restrictionHelper->getActiveDayParts();
        self::assertFalse($activeDayParts['vormittags']['hide']);
        self::assertFalse($activeDayParts['nachmittags']['hide']);

        $combinations = $restrictionHelper->getAllCombinations();
        self::assertEquals('So-1-7', $combinations[0]);
        self::assertEquals('Sa-19-24', $combinations[27]);

        self::assertFalse($restrictionHelper->isAllowed(1447919770));//Do-8
        self::assertFalse($restrictionHelper->isAllowed(1447949970)); //Do-17
        self::assertFalse($restrictionHelper->isAllowed(1447999970)); //Fr-7
        self::assertTrue($restrictionHelper->isAllowed(1448024000)); //Fr-13

        $dateTime = new \DateTimeImmutable(self::DEFAULT_TIME);
        self::assertTrue($restrictionHelper->isAtAllowedDay($dateTime)); //Fri
        self::assertFalse($restrictionHelper->isAtAllowedDay($dateTime->modify('+1 day'))); //Sa
    }

    public function testGetCalculateAppointmentTimes(): void
    {
        $restrictionHelper = new TimeRestriction('Mo-15-14'); // possible input error seems badly respected by function
        $date = new \DateTimeImmutable('2022-02-02 12:34:56');
        $appointmentHours = [
            '13:00' => ['13:00', '13:30'],
            '14:00' => ['14:10', '14:20', '14:30', '14:40'],
            '15:00' => ['15:10', '15:30', '15:50'],
            '16:00' => ['16:10', '16:30', '16:50', '16:40'],
        ];
        $calculatedTimes = $restrictionHelper->getCalculateAppointmentTimes(15, 14, $appointmentHours, $date);
        $expectedHours = '15,16,17,18,19,20,21,22,23,00,25,26,27,28,29,30,31,32,33,34,35,36,37';
        //Todo: fix the function

        self::assertSame(explode(',', $expectedHours), array_column($calculatedTimes, 'hour'));
    }

    public function testGetTimePartsOfDay(): void
    {
        $now = new \DateTimeImmutable(self::DEFAULT_TIME);
        $processList = $this->getExampleProcessList();

        $restrictionHelper = new TimeRestriction(self::TIME_RESTRICTION_STRING);
        $timePartsOfDay = $restrictionHelper->getTimesOfDay($now, $processList);

        self::assertEquals('13:00 - 18:59 Uhr', $timePartsOfDay['nachmittags']['providerList'][123]['range']);
        self::assertEquals(
            1,
            $timePartsOfDay['nachmittags']['providerList'][123]['times'][5]['free']
        );
        self::assertEquals(
            1,
            count($timePartsOfDay['nachmittags']['providerList'][123]['times'][5]['appointments'])
        );
        self::assertFalse($timePartsOfDay['nachmittags']['providerList'][123]['times'][5]['closed']);
        self::assertTrue($timePartsOfDay['nachmittags']['providerList'][123]['times'][4]['closed']);
    }

    public function  testTimeRangeDetails()
    {
        $restrictionHelper = new TimeRestriction('');
        $details = $restrictionHelper->getTimeRangeDetails();
        self::assertEquals('all', $details[0]['id']);
        self::assertEquals('Alle Tage', $details[0]['weekday']);
        self::assertEquals('Alle Tageszeiten', $details[0]['daypart']);
        self::assertEquals('Alle Zeiträume', $details[0]['timerange']);

        $restrictionHelper = new TimeRestriction(self::TIME_RESTRICTION_STRING);
        $details = $restrictionHelper->getTimeRangeDetails();
        self::assertEquals('Mo1319', $details[0]['id']);
        self::assertEquals('Montag', $details[0]['weekday']);
        self::assertEquals('nachmittags', $details[0]['daypart']);
        self::assertEquals('13:00-18:59 Uhr', $details[0]['timerange']);

        self::assertEquals('Mi1319', $details[1]['id']);
        self::assertEquals('Mittwoch', $details[1]['weekday']);
        self::assertEquals('nachmittags', $details[1]['daypart']);
        self::assertEquals('13:00-18:59 Uhr', $details[1]['timerange']);

        self::assertEquals('Fr1319', $details[2]['id']);
        self::assertEquals('Freitag', $details[2]['weekday']);
        self::assertEquals('nachmittags', $details[2]['daypart']);
        self::assertEquals('13:00-18:59 Uhr', $details[2]['timerange']);
    }

    public function testWrongHour()
    {
        self::expectException(\Exception::class);
        self::expectExceptionMessage('Could not resolve daypart for hour 24');
        $restrictionHelper = new TimeRestriction(self::TIME_RESTRICTION_STRING);
        $restrictionHelper->getDayPartForHour(24);
    }

    protected function getExampleProcessList(): ProcessList
    {
        $collection = new ProcessList();
        $entity = (new Process())->getExample();
        $collection->addEntity($entity);
        return $collection;
    }
}
