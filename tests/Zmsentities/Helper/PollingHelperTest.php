<?php

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Helper;

use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\Helper\Polling\PollingDistance;
use BO\Zmsentities\Helper\Polling\PollingDistanceList;
use PHPUnit\Framework\TestCase;

class PollingHelperTest extends TestCase
{
    public function testPollingDistanceList(): void
    {
        $fixturesPath = dirname(__FILE__) . '/../fixtures';
        $pollingReport = file_get_contents($fixturesPath .'/polling-distance-report.json');
        $result = json_decode($pollingReport, true);
        $data = $result['data'];

        $pollingDistanceList = new PollingDistanceList();
        foreach ($data as $result) {
            $pollingEntity = new PollingDistance($result);
            $pollingDistanceList[] = new PollingDistance($result);
        }
        $pollingDistanceList->setJsonCompressLevel(1);

        self::assertInstanceOf(PollingDistance::class, $pollingDistanceList[0]);
        self::assertInstanceOf(PollingDistanceList::class, $pollingDistanceList);
        self::assertTrue($pollingDistanceList[0]->getJsonCompressLevel() > 0);

        $groupedList = $pollingDistanceList->toGrouped();
        $groupedKeys = array_keys($groupedList);
        self::assertCount(1, $groupedKeys);
        self::assertSame('Bürgeramt', $groupedKeys[0]);
    }
}
