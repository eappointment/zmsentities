<?php

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Helper;

use BO\Zmsentities\Collection\AccessStatsList;
use BO\Zmsentities\Helper\Collection as CollectionHelper;
use PHPUnit\Framework\TestCase;

class CollectionTest extends TestCase
{
    public function testCreateCollectionFromArray()
    {
        $testArray = [
            [
                '$schema'    => 'https://schema.berlin.de/queuemanagement/accessStats.json',
                'id'         => 1194273554963674,
                'role'       => 'user',
                'lastActive' => 1708513257,
                'location'   => 'zmsadmin'
            ],
            [
                '$schema'    => 'https://schema.berlin.de/queuemanagement/accessStats.json',
                'id'         => 49363585614903892,
                'role'       => 'user',
                'lastActive' => 1705625839,
                'location'   => 'zmsadmin'
            ]
        ];

        self::assertInstanceOf(AccessStatsList::class, CollectionHelper::createCollectionFromArray($testArray));
    }
}
