<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Helper;

use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule;
use BO\Zmsentities\Tests\Base;
use BO\Zmsentities\Helper\MaintenanceSchedule as MaintenanceScheduleHelper;

class MaintenanceScheduleTest extends Base
{
    public function testGetNextRunTime(): void
    {
        $scheduleEntry = new MaintenanceSchedule([
            'isRepetitive' => true,
            'timeString' => '50 * * * *',
            'duration'   => 30,
        ]);

        $nextRuntime = MaintenanceScheduleHelper::getNextRunTime($scheduleEntry);
        self::assertInstanceOf(\DateTimeInterface::class, $nextRuntime);
        self::assertStringContainsString('50:00', $nextRuntime->format(DATE_ATOM));

        $scheduleEntry = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString' => '2099-12-24 12:34:56',
            'duration'   => 30,
        ]);

        $nextRuntime = MaintenanceScheduleHelper::getNextRunTime($scheduleEntry);
        self::assertInstanceOf(\DateTimeInterface::class, $nextRuntime);
        self::assertSame('2099-12-24 12:34:56', $nextRuntime->format('Y-m-d H:i:s'));

        $scheduleEntry = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString' => '1980-12-24 12:34:56',
            'duration'   => 30,
        ]);

        $nextRuntime = MaintenanceScheduleHelper::getNextRunTime($scheduleEntry);
        self::assertNull($nextRuntime);

        $scheduleEntry = new MaintenanceSchedule([
            'isRepetitive' => true,
            'timeString' => '50 * * * *',
            'duration'   => 30,
        ]);
        $currentTime = DateTime::create('2021-12-21 12:34:56');
        $nextRuntime = MaintenanceScheduleHelper::getNextRunTime($scheduleEntry, $currentTime);
        self::assertInstanceOf(\DateTimeInterface::class, $nextRuntime);
        self::assertSame('2021-12-21 12:50:00', $nextRuntime->format('Y-m-d H:i:s'));

        $scheduleEntry = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString' => '2021-12-21 23:45:00',
            'duration'   => 30,
        ]);
        $currentTime = DateTime::create('2021-12-21 12:34:56');
        $nextRuntime = MaintenanceScheduleHelper::getNextRunTime($scheduleEntry, $currentTime);
        self::assertInstanceOf(\DateTimeInterface::class, $nextRuntime);
        self::assertSame('2021-12-21 23:45:00', $nextRuntime->format('Y-m-d H:i:s'));
    }

    public function testGetNextRunEnd(): void
    {
        $scheduleEntry = new MaintenanceSchedule([
            'isRepetitive' => true,
            'timeString' => '50 * * * *',
            'duration'   => 30,
        ]);

        $nextRunEnd = MaintenanceScheduleHelper::getNextRunEnd($scheduleEntry);
        self::assertInstanceOf(\DateTimeInterface::class, $nextRunEnd);
        self::assertStringContainsString('20:00', $nextRunEnd->format(DATE_ATOM));

        $scheduleEntry = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString' => '2099-12-24 12:04:56',
            'duration'   => 30,
        ]);

        $nextRunEnd = MaintenanceScheduleHelper::getNextRunEnd($scheduleEntry);
        self::assertInstanceOf(\DateTimeInterface::class, $nextRunEnd);
        self::assertSame('2099-12-24 12:34:56', $nextRunEnd->format('Y-m-d H:i:s'));

        $scheduleEntry = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString' => '1980-12-24 12:34:56',
            'duration'   => 30,
        ]);

        $nextRunEnd = MaintenanceScheduleHelper::getNextRunEnd($scheduleEntry);
        self::assertNull($nextRunEnd);

        $scheduleEntry = new MaintenanceSchedule([
            'isRepetitive' => true,
            'timeString' => '50 * * * *',
            'duration'   => 30,
        ]);
        $currentTime = DateTime::create('2021-12-21 12:34:56');
        $nextRuntime = MaintenanceScheduleHelper::getNextRunEnd($scheduleEntry, $currentTime);
        self::assertInstanceOf(\DateTimeInterface::class, $nextRuntime);
        self::assertSame('2021-12-21 13:20:00', $nextRuntime->format('Y-m-d H:i:s'));

        $scheduleEntry = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString' => '2021-12-21 23:45:00',
            'duration'   => 30,
        ]);
        $currentTime = DateTime::create('2021-12-21 12:34:56');
        $nextRuntime = MaintenanceScheduleHelper::getNextRunEnd($scheduleEntry, $currentTime);
        self::assertInstanceOf(\DateTimeInterface::class, $nextRuntime);
        self::assertSame('2021-12-22 00:15:00', $nextRuntime->format('Y-m-d H:i:s'));
    }

    public function testGetReducedArray(): void
    {
        $scheduleEntry = new MaintenanceSchedule([
            'id'               => 42,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2021-12-21 12:34:45'),
            'startDateTime'   => DateTime::create('2021-12-21 12:34:45'),
            'isActive'         => false,
            'isRepetitive'     => false,
            'timeString'       => '2000-01-01 00:00:00',
            'duration'         => 120,
            'leadTime'         => 30,
            'area'             => 'zms',
            'announcement'     => 'bald kommts',
            'documentBody'     => 'Wartungsmodus',
        ]);

        $reducedArray = MaintenanceScheduleHelper::getReducedArray($scheduleEntry);
        $expectedRes  = [
            'startDateTime'   => '2021-12-21T12:34:45+01:00',
            'isActive'         => false,
            'isRepetitive'     => false,
            'timeString'       => '2000-01-01 00:00:00',
            'duration'         => 120,
            'leadTime'         => 30,
            'area'             => 'zms',
            'id'               => 42,
        ];

        self::assertSame($expectedRes, $reducedArray);
    }
}
