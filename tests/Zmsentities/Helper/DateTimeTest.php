<?php

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Helper;

use BO\Zmsentities\Helper\DateTime;
use PHPUnit\Framework\TestCase;

class DateTimeTest extends TestCase
{
    public function testSetTimestamp(): void
    {
        $datetime = new DateTime();

        self::assertInstanceOf(DateTime::class, $datetime);

        $datetime = $datetime->setTimestamp(1234567);

        self::assertInstanceOf(DateTime::class, $datetime);
    }
}
