<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Helper;

use BO\Zmsentities\Calldisplay;
use BO\Zmsentities\Helper\ApplicationRegister as AppRegHelper;
use BO\Zmsentities\ApplicationRegister as Entity;
use PHPUnit\Framework\TestCase;

class ApplicationRegisterTest extends TestCase
{
    public function testGetEntityByCalldisplay(): void
    {
        $calldisplay = (new Calldisplay([
            'hash'   => 'hbwee6ae7fa8ff01bcfdabcecf722abe049AmPaD',
            'status' => [
                "agent"      => "Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0",
                "template"   => "default",
                "qrcode"     => 1,
                "createDate" => "2016-04-01 12:34:56",
                "lastDate"   => "2016-04-01",
                "daysActive" => 1,
            ],
        ]))->withResolvedCollections([
            'scopelist'   => '140,141',
            'clusterlist' => '75,76',
        ]);

        $regEntity = AppRegHelper::getEntityByCalldisplay($calldisplay);

        self::assertSame('hbwee6ae7fa8ff01bcfdabcecf722abe049AmPaD', $regEntity->id);
        self::assertSame('calldisplay', $regEntity->type);
        self::assertSame(
            'collections[scopelist]=140,141&collections[clusterlist]=75,76&template=default&qrcode=1',
            $regEntity->parameters
        );
        self::assertSame('Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0', $regEntity->userAgent);
        self::assertSame(141, $regEntity->scopeId);
        self::assertSame(1, $regEntity->daysActive);

        $calldisplay->hash = 'afe84ad6c8de6a412e1f584eg6af78e0';

        $regEntity = AppRegHelper::getEntityByCalldisplay($calldisplay);

        self::assertSame(140, $regEntity->scopeId);
    }

    public function testHasChangedAsRequested(): void
    {
        $entity = new Entity([
            'id' => '12345678',
            'type' => 'calldisplay',
            'parameters' => 'collections[clusterlist]=110&template=defaultplatz',
            'userAgent' => 'Browser',
            'scopeId'  => 1234,
            'startDate' => '2001-01-01 12:34:56',
            'lastDate'  => new \DateTime('2001-01-01'),
            'daysActive' => 1,
            'requestedChange' => [
                'parameters' => 'collections[clusterlist]=110&collections[scopelist]=141,140,142&template=defaultplatz'
            ],
        ]);

        self::assertFalse(AppRegHelper::hasChangedAsRequested($entity));

        $entity->parameters = 'collections[scopelist]=140,141,142&collections[clusterlist]=110&template=defaultplatz';

        self::assertTrue(AppRegHelper::hasChangedAsRequested($entity));
    }
}
