<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Collection;

use BO\Zmsentities\AccessStats;
use BO\Zmsentities\Collection\AccessStatsList;
use PHPUnit\Framework\TestCase;

class AccessStatsListTest extends TestCase
{
    public function testGetCountedGroupBy(): void
    {
        $collection = new AccessStatsList([
            new AccessStats([
                'id' => 6846151686468,
                'role' => 'user',
                'lastActive' => 1234567890,
                'location' => 'zmsadmin',
            ]),
            new AccessStats([
                'id' => 8461351843154,
                'role' => 'citizen',
                'lastActive' => 1234567890,
                'location' => 'zmsappointment',
            ]),
            new AccessStats([
                'id' => 1866461315389,
                'role' => 'citizen',
                'lastActive' => 1234567890,
                'location' => 'zmsappointment',
            ]),
        ]);

        $groupSums = $collection->getCountedGroupBy('role');

        self::assertSame(['user' => 1, 'citizen' => 2], $groupSums);
    }
}
