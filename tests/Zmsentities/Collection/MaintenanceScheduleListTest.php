<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Collection;

use BO\Zmsentities\Collection\MaintenanceScheduleList;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\Helper\MaintenanceSchedule as ScheduleHelper;
use BO\Zmsentities\MaintenanceSchedule;
use PHPUnit\Framework\TestCase;

class MaintenanceScheduleListTest extends TestCase
{
    public function testSortByEndTime()
    {
        $now = DateTime::create('2021-12-12 01:00:00');
        $list = new MaintenanceScheduleList();

        $entity1 = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString'   => '2021-12-12 12:34:56',
            'duration'     => 30
        ]);

        $list->addEntity($entity1);
        self::assertSame('2021-12-12 13:04:56', ScheduleHelper::getNextRunEnd($entity1, $now)->format('Y-m-d H:i:s'));

        $entity2 = new MaintenanceSchedule([
            'isRepetitive' => true,
            'timeString'   => '5 2 * * 7',
            'duration'     => 30
        ]);

        $list->addEntity($entity2);
        self::assertSame('2021-12-12 02:35:00', ScheduleHelper::getNextRunEnd($entity2, $now)->format('Y-m-d H:i:s'));

        $entity3 = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString'   => '2021-12-12 12:34:56',
            'duration'     => 60
        ]);

        $list->addEntity($entity3);
        self::assertSame('2021-12-12 13:34:56', ScheduleHelper::getNextRunEnd($entity3, $now)->format('Y-m-d H:i:s'));

        $list->sortByScheduleEndTime($now);
        $listArray = (array)$list;
        $nextEntry = reset($listArray);

        self::assertNull($list->getActiveEntry());
        self::assertSame('5 2 * * 7', $nextEntry->getTimeString());

        $entity2['duration'] = 720;
        $list->sortByScheduleEndTime($now);
        $listArray = (array)$list;
        $nextEntry = reset($listArray);
        self::assertSame('2021-12-12 12:34:56', $nextEntry->getTimeString());

        $entity4 = new MaintenanceSchedule([
            'isActive'     => true,
            'isRepetitive' => true,
            'timeString'   => '*/2 * * * *',
            'duration'     => 30
        ]);

        $list->addEntity($entity4);

        self::assertInstanceOf(MaintenanceSchedule::class, $list->getActiveEntry());

        $list->sortByScheduleEndTime($now);
        $listArray = (array)$list;
        $nextEntry = reset($listArray);

        self::assertSame('*/2 * * * *', $nextEntry->getTimeString());

        //test concurrency
        $list = new MaintenanceScheduleList();
        $list->addEntity(new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString'   => '2021-12-12 12:30:00',
            'duration'     => 60
        ]));
        $list->addEntity(new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString'   => '2021-12-12 13:00:00',
            'duration'     => 30
        ]));

        $list->sortByScheduleEndTime($now);
        $listArray = (array)$list;
        $nextEntry = reset($listArray);
        self::assertSame('2021-12-12 12:30:00', $nextEntry->getTimeString());

        // type test
        $list->sortByScheduleEndTime();
    }

    public function testSortByStartTime(): void
    {
        $now = DateTime::create('2021-12-12 01:00:00');
        $list = new MaintenanceScheduleList();

        $entity1 = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString'   => '2021-12-12 12:34',
            'duration'     => 30
        ]);

        $list->addEntity($entity1);
        self::assertSame('2021-12-12 12:34:00', ScheduleHelper::getNextRunTime($entity1, $now)->format('Y-m-d H:i:s'));

        $entity2 = new MaintenanceSchedule([
            'isRepetitive' => true,
            'timeString'   => '5 22 * * 7',
            'duration'     => 30
        ]);

        $list->addEntity($entity2);
        self::assertSame('2021-12-12 22:05:00', ScheduleHelper::getNextRunTime($entity2, $now)->format('Y-m-d H:i:s'));

        $entity3 = new MaintenanceSchedule([
            'isRepetitive' => true,
            'timeString'   => '34 12 * * *',
            'duration'     => 60
        ]);

        $list->addEntity($entity3);
        self::assertSame('2021-12-12 12:34:00', ScheduleHelper::getNextRunTime($entity3, $now)->format('Y-m-d H:i:s'));

        $list->sortByScheduleStartTime($now);
        $listArray = (array)$list;
        $nextEntry = reset($listArray);

        self::assertNull($list->getActiveEntry());
        self::assertSame('34 12 * * *', $nextEntry->getTimeString());

        // type test
        $list->sortByScheduleStartTime();
    }
}