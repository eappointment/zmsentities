<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Schema;

use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\Schema\UnflattedArray;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

class UnflattedArrayTest extends TestCase
{
    use ProphecyTrait;

    public function testTestValidArrayKey(): void
    {
        $this->expectNotToPerformAssertions();
        UnflattedArray::testValidArrayKey('key');
        UnflattedArray::testValidArrayKey(123);
    }

    public function testTestValidArrayKeyFails(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        UnflattedArray::testValidArrayKey(true);
    }

    public function testCopyBoolean(): void
    {
        $negativeProphecy = $this->prophesize(UnflattedArray::class);
        $negativeProphecy->offsetSet(Argument::any(), Argument::any())->shouldNotBeCalled();
        $neg = $negativeProphecy->reveal();

        $pos1 = $this->prophesize(UnflattedArray::class);
        $pos1->offsetSet(Argument::type('string'), Argument::type('bool'))->shouldBeCalled();
        $pos2 = clone $pos1;

        $sut = new UnflattedArray(['key' => 'value']);
        $sut->copyBoolean('notExistingKey', $neg);
        $sut->copyBoolean('key', $neg, false);
        $sut->copyBoolean('key', $pos1->reveal(), true);

        $sut = new UnflattedArray(['boolean' => true]);
        $sut->copyBoolean('boolean', $pos2->reveal(), false);
    }

    public function testCopyInteger(): void
    {
        $negativeProphecy = $this->prophesize(UnflattedArray::class);
        $negativeProphecy->offsetSet(Argument::any(), Argument::any())->shouldNotBeCalled();
        $neg = $negativeProphecy->reveal();

        $pos1 = $this->prophesize(UnflattedArray::class);
        $pos1->offsetSet(Argument::type('string'), Argument::type('int'))->shouldBeCalled();
        $pos2 = clone $pos1;

        $sut = new UnflattedArray(['key' => 'value']);
        $sut->copyInteger('notExistingKey', $neg);
        $sut->copyInteger('key', $neg, false);
        $sut->copyInteger('key', $pos1->reveal(), true);

        $sut = new UnflattedArray(['integer' => 123]);
        $sut->copyInteger('integer', $pos2->reveal(), false);
    }

    public function testCopyString(): void
    {
        $negativeProphecy = $this->prophesize(UnflattedArray::class);
        $negativeProphecy->offsetSet(Argument::any(), Argument::any())->shouldNotBeCalled();
        $neg = $negativeProphecy->reveal();

        $pos1 = $this->prophesize(UnflattedArray::class);
        $pos1->offsetSet(Argument::type('string'), Argument::type('string'))->shouldBeCalled();
        $pos2 = clone $pos1;

        $sut = new UnflattedArray(['key' => 123]);
        $sut->copyString('notExistingKey', $neg);
        $sut->copyString('key', $neg, false);
        $sut->copyString('key', $pos1->reveal(), true);

        $sut = new UnflattedArray(['string' => 'true']);
        $sut->copyString('boolean', $pos2->reveal(), false);
    }

    public function testCopyDateTime(): void
    {
        $negativeProphecy = $this->prophesize(UnflattedArray::class);
        $negativeProphecy->offsetSet(Argument::any(), Argument::any())->shouldNotBeCalled();
        $neg = $negativeProphecy->reveal();

        $pos1 = $this->prophesize(UnflattedArray::class);
        $pos1->offsetSet(Argument::type('string'), Argument::type(DateTime::class))->shouldBeCalled();
        $pos2 = clone $pos1;

        $sut = new UnflattedArray(['key' => '2002-02-02 12:34:56']);
        $sut->copyDateTime('notExistingKey', $neg);
        $sut->copyDateTime('key', $neg, false);
        $sut->copyDateTime('key', $pos1->reveal(), true);

        $sut = new UnflattedArray(['dateTime' => true]);
        $sut->copyDateTime('dateTime', $pos2->reveal(), false);
    }
}
