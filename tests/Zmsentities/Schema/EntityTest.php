<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Schema;

use BO\Zmsentities\Schema\Entity;
use PHPUnit\Framework\TestCase;

class EntityTest extends TestCase
{
    public function testWithData(): void
    {
        $sut = new Entity(['some' =>  'data']);
        $res = $sut->withData(['merged' => 'value']);

        $expected = [
            'some' =>  'data',
            'merged' => 'value',
        ];

        self::assertSame($expected, (array) $res);
    }

    public function testWithProperty(): void
    {
        $sut = new Entity(['some' =>  'data']);
        $res = $sut->withProperty('added', 'value');

        $expected = [
            'some' =>  'data',
            'added' => 'value',
        ];

        self::assertSame($expected, (array) $res);
    }
}
