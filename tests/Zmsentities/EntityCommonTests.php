<?php

namespace BO\Zmsentities\Tests;

use BO\Zmsentities\Schema\Entity;

abstract class EntityCommonTests extends Base
{

    public function testNew()
    {
        $example = $this->getExample();
        $this->assertTrue($example->testValid());
    }

    public function getExample(): Entity
    {
        $entity = new $this->entityclass();

        return $entity::getExample();
    }

    public function assertEntity($entityClass, $entity)
    {
        $this->assertInstanceOf($entityClass, $entity);
        $entity->testValid();
    }

    public function assertEntityList($entityClass, $entityList)
    {
        foreach ($entityList as $entity) {
            $this->assertEntity($entityClass, $entity);
        }
    }

    public function testLessData()
    {
        $example = $this->getExample()->withLessData();
        $this->assertTrue($example->testValid());
    }
}
