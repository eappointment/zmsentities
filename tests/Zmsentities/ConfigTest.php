<?php

namespace BO\Zmsentities\Tests;

use BO\Zmsentities\Config;

class ConfigTest extends EntityCommonTests
{
    public $entityclass = '\BO\Zmsentities\Config';

    public function testBasic()
    {
        /** @var Config $entity */
        $entity = $this->getExample();
        $this->assertFalse($entity->getNotificationPreferences()['absage'], 'config notifications not accessible');
        $this->assertFalse($entity->getPreference('notifications', 'absage'), 'config getPreference failed');
        $entity->setPreference('notifications', 'absage', true);
        $this->assertTrue($entity->hasType('notifications'), 'config hasType failed');
        $this->assertTrue($entity->hasPreference('notifications', 'absage'), 'config hasPreference failed');
        $this->assertTrue($entity->getPreference('notifications', 'absage'), 'config setPreference failed');
        $this->assertTrue($entity->getEntryOrDefault('notifications__absage'), 'config getEntryOrDefault failed');
        $this->assertFalse($entity->hasEntry('foo__bar'), 'config hasEntry failed');
        $this->assertNull($entity->getEntryOrDefault('foo__bar'), 'config getEntryOrDefault failed');
    }

    public function testMerge()
    {
        /** @var Config $example */
        $example = $this->getExample();
        $example->addData(['emergency' => ['refreshInterval' => 10]]);
        $this->assertEquals(10, $example['emergency']['refreshInterval']);
        $this->assertTrue($example->testValid());
    }

    public function testHasEntryException(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        /** @var Config $example */
        $example = $this->getExample();
        $example->hasEntry('');
    }
}
