<?php

declare(strict_types=1);

namespace BO\Zmsentities\Tests;

use BO\Zmsentities\CheckInTask;
use BO\Zmsentities\Process;
use BO\Zmsentities\Schema\Entity;

class CheckInTaskTest extends Base
{
    public $entityclass = '\BO\Zmsentities\CheckInTask';

    public function testExample(): void
    {
        $sut = CheckInTask::getExample();

        self::assertTrue($sut->isValid());
        self::assertIsInt($sut->getId());
        self::assertIsInt($sut->referenceId);
        self::assertIsInt($sut->scopeId);
        self::assertIsInt($sut->checkInTS);
        self::assertIsInt($sut->scheduledTS);
        self::assertIsString($sut->checkInSource);
    }

    public function testAddData(): void
    {
        $sut = new CheckInTask([
            'status' => 'open',
            'refersTo' => 'Process',
            'referenceId' => 123456,
            'scopeId' => 123,
            'checkInTS' => 1433333000,
            'scheduledTS' => 1433333333,
        ]);

        self::assertSame('2015-06-03T14:08:53+02:00', $sut->scheduledDateTime->format(DATE_ATOM));
        self::assertSame('2015-06-03T14:03:20+02:00', $sut->checkInDateTime->format(DATE_ATOM));
        self::assertIsString($sut->checkInSource);
        self::assertNull($sut->lastChange);

        $sut = new CheckInTask([
            'referenceId' => 345678,
            'scopeId' => 345,
            'updatedTimestamp' => 1434567890,
        ]);

        self::assertSame(1434567890, $sut->updatedTimestamp);
    }

    public function testProcessValidation(): void
    {
        $process = Process::getExample();
        $jsonString = json_encode($process->jsonSerialize());

        self::assertStringContainsString('"checkInTask":{', $jsonString);
        self::assertTrue($process->isValid());

        $process = Process::getExample();
        $process->checkInTask = null;

        self::assertTrue($process->isValid());
    }
}
