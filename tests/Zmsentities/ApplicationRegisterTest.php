<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests;

use BO\Zmsentities\ApplicationRegister;
use BO\Zmsentities\Exception\SchemaValidation;

class ApplicationRegisterTest extends EntityCommonTests
{
    public $entityclass = '\BO\Zmsentities\ApplicationRegister';

    public function testSerialization(): void
    {
        $entity = new ApplicationRegister();
        $entity->addData([
            'id' => '00000000-0000-0000-0000-000000000000',
            'type' => 'ticketprinter',
            'parameters' => '/ticketprinter/scope/123/',
            'startDate' => '2001-01-01 12:34:56',
            'lastDate'  => new \DateTime('2001-01-01'),
        ]);

        $expected = '{';
        $expected .=   '"$schema":"https:\/\/schema.berlin.de\/queuemanagement\/applicationRegister.json",';
        $expected .=   '"type":"ticketprinter",';
        $expected .=   '"parameters":"\/ticketprinter\/scope\/123\/",';
        $expected .=   '"userAgent":null,';
        $expected .=   '"scopeId":null,';
        $expected .=   '"startDate":"2001-01-01 12:34:56",';
        $expected .=   '"lastDate":"2001-01-01",';
        $expected .=   '"daysActive":1,';
        $expected .=   '"requestedChange":null,';
        $expected .=   '"id":"00000000-0000-0000-0000-000000000000"';
        $expected .=   '}';

        self::assertSame($expected, $entity->__toString());

        $entity->addData(['scopeId' => '141', 'daysActive' => '2']);

        self::assertSame([
            '$schema' => 'https://schema.berlin.de/queuemanagement/applicationRegister.json',
            'type' => 'ticketprinter',
            'parameters' => '/ticketprinter/scope/123/',
            'userAgent' => null,
            'scopeId' => 141,
            'startDate' => '2001-01-01 12:34:56',
            'lastDate'  => '2001-01-01',
            'daysActive' => 2,
            'requestedChange' => null,
            'id' => '00000000-0000-0000-0000-000000000000',
        ], json_decode($entity->__toString(), true));
    }

    public function testValidationSucceeds(): void
    {
        $entity = new ApplicationRegister([
            'id' => '12345678',
            'type' => 'calldisplay',
        ]);

        self::assertTrue($entity->testValid());

        $entity = new ApplicationRegister([
            'id' => '12345678',
            'type' => 'calldisplay',
            'parameters' => 'collections[clusterlist]=110&template=defaultplatz',
            'userAgent' => 'Browser',
            'scopeId'  => 1234,
            'startDate' => '2001-01-01 12:34:56',
            'lastDate'  => new \DateTime('2001-01-01'),
            'daysActive' => 1,
            'requestedChange' => [
                'parameters' => 'collections[clusterlist]=110&collections[scopelist]=380&template=defaultplatz'
            ],
        ]);

        $entity->addChangeRequest(
            'parameters',
            'collections[scopelist]=380&template=defaultplatz'
        );

        self::assertSame(
            ['parameters' => 'collections[scopelist]=380&template=defaultplatz'],
            $entity->getChangeRequest()
        );

        self::assertTrue($entity->testValid());
        self::assertTrue($entity->hasChangeRequest());

        $entity->clearChangeRequest();

        self::assertTrue($entity->testValid());
        self::assertFalse($entity->hasChangeRequest());
    }

    public function testValidationFails(): void
    {
        $this->expectException(SchemaValidation::class);

        $entity = new ApplicationRegister([]);
        $entity->testValid();
    }
}
