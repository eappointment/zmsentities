<?php

namespace BO\Zmsentities;

use BO\Zmsentities\Collection\DepartmentList;
use BO\Zmsentities\Helper\Property;

/**
 * @SuppressWarnings(Complexity)
 * @SuppressWarnings(PublicMethod)
 * @SuppressWarnings(Coupling)
 */
class Useraccount extends Schema\Entity
{
    const PRIMARY = 'id';

    public static $schema = "useraccount.json";

    public function getDefaults()
    {
        return [
            'rights' => [
                "availability" => false,
                "basic" => true,
                "cluster" => false,
                "department" => false,
                "organisation" => false,
                "scope" => false,
                "sms" => false,
                "superuser" => false,
                "ticketprinter" => false,
                "useraccount" => false,
            ],
            'departments' => new Collection\DepartmentList(),
        ];
    }

    /**
     * @return bool
     * @throws Exception\UserAccountMissingProperties
     */
    public function hasProperties(): bool
    {
        foreach (func_get_args() as $property) {
            if (!$this->toProperty()->$property->get()) {
                throw new Exception\UserAccountMissingProperties("Missing property " . htmlspecialchars($property));
            }
        }
        return true;
    }

    public function getDepartmentList(): DepartmentList
    {
        if (!$this->departments instanceof DepartmentList) {
            $this->departments = new DepartmentList($this->departments);
            foreach ($this->departments as $key => $department) {
                $this->departments[$key] = new Department($department);
            }
        }
        return $this->departments;
    }

    public function addDepartment($department): Useraccount
    {
        $this->departments[] = $department;
        return $this;
    }

    public function getDepartment($departmentId): Department
    {
        if (count($this->departments)) {
            foreach ($this->getDepartmentList() as $department) {
                if ($department['id'] == $departmentId) {
                    return $department;
                }
            }
        }
        return new Department(['name' => 'Not existing']);
    }

    public function hasDepartment($departmentId): bool
    {
        return $this->getDepartment($departmentId)->hasId();
    }

    public function hasScope($scopeId): bool
    {
        return $this->getDepartmentList()->getUniqueScopeList()->hasEntity($scopeId);
    }

    /**
     * @todo Remove this function, keep no contraint on old DB schema in zmsentities
     */
    public function getRightsLevel()
    {
        return Helper\RightsLevelManager::getLevel($this->rights);
    }

    public function setRights(): Useraccount
    {
        $givenRights = func_get_args();
        foreach ($givenRights as $right) {
            if (Property::__keyExists($right, $this->rights)) {
                $this->rights[$right] = true;
            }
        }
        return $this;
    }

    public function hasRights(array $requiredRights): bool
    {
        if ($this->isSuperUser()) {
            return true;
        }
        foreach ($requiredRights as $required) {
            if ($required instanceof Useraccount\RightsInterface) {
                if (!$required->validateUseraccount($this)) {
                    return false;
                }
            } elseif (! $this->toProperty()->rights->$required->get()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param array $requiredRights
     * @return $this
     * @throws Exception\UserAccountMissingLogin
     * @throws Exception\UserAccountMissingRights
     */
    public function testRights(array $requiredRights): Useraccount
    {
        if ($this->hasId()) {
            if (!$this->hasRights($requiredRights)) {
                throw new Exception\UserAccountMissingRights(
                    "Missing rights " . htmlspecialchars(implode(',', $requiredRights))
                );
            }
        } else {
            throw new Exception\UserAccountMissingLogin();
        }
        return $this;
    }

    public function isOveraged(\DateTimeInterface $dateTime): bool
    {
        if (Property::__keyExists('lastLogin', $this)) {
            $lastLogin = (new \DateTimeImmutable())->setTimestamp($this['lastLogin'])->modify('23:59:59');
            return ($lastLogin < $dateTime);
        }
        return false;
    }

    public function isSuperUser()
    {
        return $this->toProperty()->rights->superuser->get();
    }

    public function getDepartmentById($departmentId): ?Department
    {
        foreach ($this->departments as $department) {
            if ($departmentId == $department['id']) {
                return new Department($department);
            }
        }

        return null;
    }

    public function testDepartmentById($departmentId): Department
    {
        $department = $this->getDepartmentById($departmentId);
        if (!$department) {
            throw new Exception\UserAccountMissingDepartment(
                "Missing department " . htmlspecialchars($departmentId)
            );
        }
        return $department;
    }

    public function setPassword($input): Useraccount
    {
        if (isset($input['password']) && '' != $input['password']) {
            $this->password = $input['password'];
        }
        if (isset($input['changePassword']) && 0 < count(array_filter($input['changePassword']))) {
            if (! isset($input['password'])) {
                $this->password = $input['changePassword'][0];
            }
            $this->changePassword = $input['changePassword'];
        }
        return $this;
    }

    public function withDepartmentList(): Useraccount
    {
        $departmentList = new Collection\DepartmentList();
        $entity = clone $this;
        foreach ($this->departments as $department) {
            if (! is_array($department) && ! $department instanceof Department) {
                $department = new Department(array('id' => $department));
            }
            $departmentList->addEntity($department);
        }
        $entity->departments = $departmentList;
        return $entity;
    }

    public function withCleanedUpFormData($keepPassword = false)
    {
        if (isset($this['save'])) {
            unset($this['save']);
        }
        if (isset($this['password']) && '' == $this['password'] && false === $keepPassword) {
            unset($this['password']);
        }
        if (isset($this['changePassword']) &&
            0 == count(array_filter($this['changePassword'])) &&
            false === $keepPassword
        ) {
            unset($this['changePassword']);
        }
        if (isset($this['oidcProvider'])) {
            unset($this['oidcProvider']);
        }

        return $this;
    }

    /**
     * verify hashed password and create new if needs rehash
    */
    public function setVerifiedHash(string $password): Useraccount
    {
        // Do you have old, turbo-legacy, non-crypt hashes?
        if (strpos($this->password, '$') !== 0) {
            //error_log(__METHOD__ . "::legacy_hash\n");
            $result = $this->password === md5($password);
        } else {
            //error_log(__METHOD__ . "::password_verify\n");
            $result = password_verify($password, $this->password);
        }

        // on passed validation check if the hash needs updating.
        if ($result && $this->isPasswordNeedingRehash()) {
            $this->password = $this->getHash($password);
            //error_log(__METHOD__ . "::rehash\n");
        }

        return $this;
    }

    public function withVerifiedHash(string $password): Useraccount
    {
        $useraccount = clone $this;
        if ($useraccount->isPasswordNeedingRehash()) {
            $useraccount->setVerifiedHash($password);
        }
        return $useraccount;
    }

    public function isPasswordNeedingRehash(): bool
    {
        return password_needs_rehash($this->password, PASSWORD_DEFAULT);
    }

    /**
     * set salted hash by string
    */
    public static function getHash(string $string): string
    {
        return password_hash($string, PASSWORD_DEFAULT);
    }

    /**
     * create user account from open id input data with random password
    */
    public static function createFromOpenidData($data): Useraccount
    {
        $entity = new self();
        $entity->id = $data['username'];
        $entity->email = $data['email'];
        $department = new Department(['id' => 0]);
        $entity->addDepartment($department);
        $password = substr(str_shuffle($entity->id . $entity->email), 0, 8);
        $entity->password = self::getHash($password);

        return $entity;
    }

    /**
     * get oidc provider from $entity id if it exists
     *
     * @return string $entity
    */
    public function getOidcProviderFromName(): ?string
    {
        $providerName = '';
        if (($pos = strpos($this->id, "@")) !== false) {
            $providerName = substr($this->id, $pos+1);
        }
        return ('' !== $providerName) ? $providerName : null;
    }
}
