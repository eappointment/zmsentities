<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities;

use BO\Zmsentities\Exception\SchemaValidation;
use BO\Zmsentities\Helper\DateTime;
use DateTimeInterface;

/**
 * register for calldisplay and ticketprinter
 *
 * @property string $id
 * @property string $type
 * @property string|null $parameters (parsed from string used by API)
 * @property string|null $userAgent
 * @property int|null $scopeId
 * @property DateTimeInterface $startDate
 * @property DateTimeInterface $lastDate
 * @property int $daysActive
 * @property array|null $requestedChange
 */
class ApplicationRegister extends Schema\Entity
{
    public const PRIMARY = 'id';

    public const TYPE_ZMS1_CALLDISPLAY = 'calldisplay-alt';
    public const TYPE_ZMS1_TICKETPRINTER = 'ticketprinter-alt';
    public const TYPE_ZMS2_CALLDISPLAY = 'calldisplay';
    public const TYPE_ZMS2_CHECKINCOUNTER = 'check-in';
    public const TYPE_ZMS2_TICKETPRINTER = 'ticketprinter';

    public static $schema = "applicationRegister.json";

    public function getEntityName()
    {
        return 'applicationRegister';
    }

    public function getDefaults()
    {
        $now = class_exists('\App') && property_exists('\App', 'now') ? \App::$now : new \DateTime();

        return [
            'type' => 'undefined',
            'parameters' => null,
            'userAgent' => null,
            'scopeId' => null,
            'startDate' => $now,
            'lastDate' => $now,
            'daysActive' => 1,
            'requestedChange' => null,
        ];
    }

    /**
     * @SuppressWarnings(Complexity)
     * {@inheritDoc}
     */
    public function addData($mergeData): Schema\Entity
    {
        if (isset($mergeData['startDate']) && is_string($mergeData['startDate'])) {
            $mergeData['startDate'] = new DateTime($mergeData['startDate']);
        }
        if (isset($mergeData['lastDate']) && is_string($mergeData['lastDate'])) {
            $mergeData['lastDate'] = new DateTime($mergeData['lastDate']);
        }
        if (isset($mergeData['scopeId']) && is_string($mergeData['scopeId'])) {
            $mergeData['scopeId'] = (int) $mergeData['scopeId'];
        }
        if (isset($mergeData['daysActive']) && is_string($mergeData['daysActive'])) {
            $mergeData['daysActive'] = (int) $mergeData['daysActive'];
        }
        if (isset($mergeData['requestedChange']) && is_string($mergeData['requestedChange'])) {
            if (strlen($mergeData['requestedChange']) === 0) {
                $mergeData['requestedChange'] = null;
            } else {
                try {
                    $mergeData['requestedChange'] = json_decode($mergeData['requestedChange'], true);
                } catch (\Exception $e) {
                    throw new SchemaValidation($e->getMessage(), 400, $e);
                }
            }
        }

        return parent::addData($mergeData);
    }

    public function getChangeRequest(): ?array
    {
        return is_array($this->requestedChange) ? $this->requestedChange : null;
    }

    public function addChangeRequest($key, $newValue): ApplicationRegister
    {
        $requestedChange = $this->getChangeRequest() ?? [];
        $requestedChange[$key] = $newValue;

        $this->requestedChange = $requestedChange;

        return $this;
    }

    public function hasChangeRequest(): bool
    {
        return (is_array($this->requestedChange));
    }

    public function clearChangeRequest()
    {
        $this->requestedChange = null;
    }

    public function jsonSerialize()
    {
        $data = (array) $this;

        foreach ($data as $key => $value) {
            if ($value instanceof DateTimeInterface) {
                $data[$key] = $value->format('Y-m-d' . ($key === 'startDate' ? ' H:i:s' : ''));
            }
        }

        return array_merge(['$schema' => Schema\Schema::$baseUrl . self::$schema], $data);
    }
}
