<?php

namespace BO\Zmsentities;

class RemoteCall extends Schema\Entity
{
    public const TYPE_CHANGE = "change";
    public const TYPE_DELETE = "delete";
    public const TYPE_DETAIL = "detail";
    public const PRIMARY = 'id';

    public static $schema = "remotecall.json";

    public function getDefaults(): array
    {
        return [
            "processArchiveId" => "",
            "name" => "detail",
            "url" => "bda://bda.service.berlin.de/detail/?",
            "call" => [
                "count"=> 0,
                "requested" => 0,
                "statuscode" => 0,
            ],
        ];
    }

    public function isTypeAllowed(): bool
    {
        return in_array($this->name, [self::TYPE_CHANGE, self::TYPE_DELETE, self::TYPE_DETAIL]);
    }
}
