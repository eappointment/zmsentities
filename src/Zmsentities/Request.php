<?php

namespace BO\Zmsentities;

class Request extends Schema\Entity
{
    const PRIMARY = 'id';

    public static $schema = "request.json";

    public function addData($mergeData)
    {
        if (isset($mergeData['data']) && is_string($mergeData['data'])) {
            if (strlen($mergeData['data']) < 3) {
                unset($mergeData['data']);
            } else {
                try {
                    $mergeData['data'] = json_decode($mergeData['data']);
                } catch (\Exception $e) {
                    unset($mergeData['data']);
                }
            }
        }

        return parent::addData($mergeData);
    }

    public function withReference($additionalData = [])
    {
        $additionalData['id'] = $this->getId();
        $additionalData['name'] = $this->getName();
        return parent::withReference($additionalData);
    }

    public function hasAppointmentFromProviderData()
    {
        if (isset($this['data']) && isset($this['data']['locations'])) {
            foreach ($this['data']['locations'] as $provider) {
                if ((!isset($provider['appointment']['external']) || !$provider['appointment']['external'])
                    && isset($provider['appointment']['allowed']) && $provider['appointment']['allowed']
                ) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getSource()
    {
        return $this->toProperty()->source->get();
    }

    public function getGroup()
    {
        return $this->toProperty()->group->get();
    }

    public function getLink()
    {
        return $this->toProperty()->link->get();
    }

    public function getName()
    {
        return $this->toProperty()->name->get();
    }

    public function getAdditionalData()
    {
        return $this->toProperty()->data->get();
    }
}
