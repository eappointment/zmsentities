<?php

namespace BO\Zmsentities;

class Mimepart extends Schema\Entity
{
    public const PRIMARY = 'id';

    public const MIME_TYPE_XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    public const MIME_TYPE_HTML = 'text/html';
    public const MIME_TYPE_TEXT = 'text/plain';
    public const MIME_TYPE_ICS = 'text/calendar';

    public static $schema = "mimepart.json";

    public function getDefaults()
    {
        return [
            'mime' => '',
            'attachmentName' => '',
            'content' => '',
            'base64' => false,
        ];
    }

    public function isBase64Encoded(): bool
    {
        return ($this->base64) ? true : false;
    }

    public function isHtml(): bool
    {
        return ($this->mime == self::MIME_TYPE_HTML) ? true : false;
    }

    public function isText(): bool
    {
        return ($this->mime == self::MIME_TYPE_TEXT) ? true : false;
    }

    public function isIcs(): bool
    {
        return ($this->mime == self::MIME_TYPE_ICS) ? true : false;
    }

    public function isXlsx(): bool
    {
        return ($this->mime == self::MIME_TYPE_XLSX) ? true : false;
    }

    public function getContent()
    {
        return ($this->isBase64Encoded()) ? \base64_decode($this->content) : $this->content;
    }

    public function getExtension()
    {
        return preg_replace('#^.*/#', '', $this->mime);
    }
}
