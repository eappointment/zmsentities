<?php
namespace BO\Zmsentities\Collection;

use BO\Zmsentities\RemoteCall as Entity;

class RemoteCallList extends Base
{
    const ENTITY_CLASS = '\BO\Zmsentities\RemoteCall';

    /**
     * @param RemoteCallList $updateDataList
     *
     * @return $this|RemoteCallList
     */
    public function withUpdateData(self $updateDataList): RemoteCallList
    {
        $list = new self();
        foreach ($updateDataList as $updateEntity) {
            if ($updateEntity->hasId() && $this->hasEntity($updateEntity->getId())) {
                $entity = $this->getEntity($updateEntity->getId());
            } elseif ($this->getMatchingByNameAndUrl($updateEntity) !== null) {
                $entity = $this->getMatchingByNameAndUrl($updateEntity);
            } else {
                $entity = $updateEntity;
            }
            $entity->name = $updateEntity->name;
            $entity->url = $updateEntity->url;
            $list->addEntity($entity);
        }
        return $list;
    }

    /**
     * @param $matchingEntity
     *
     * @return Entity|null
     */
    public function getMatchingByNameAndUrl($matchingEntity): ?Entity
    {
        foreach ($this as $entity) {
            if (isset($entity['name']) &&
                isset($entity['url']) &&
                isset($matchingEntity['name']) &&
                isset($matchingEntity['url']) &&
                $entity->name == $matchingEntity->name &&
                $entity->url == $matchingEntity->url
            ) {
                return $entity;
            }
        }
        return null;
    }

    public function getByName(string $name): ?Entity
    {
        foreach ($this as $entity) {
            if ($entity->name == $name) {
                return $entity;
            }
        }
        return null;
    }

    public function setProcessArchiveId(string $processArchiveId = ''): RemoteCallList
    {
        if ($processArchiveId == '') {
            return $this;
        }
        foreach ($this as $entity) {
            $entity->processArchiveId = $processArchiveId;
        }
        return $this;
    }
}
