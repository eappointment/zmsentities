<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Collection;

class SessionList extends Base
{
    const ENTITY_CLASS = '\BO\Zmsentities\Session';
}
