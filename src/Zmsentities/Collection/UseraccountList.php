<?php

namespace BO\Zmsentities\Collection;

use BO\Zmsentities\Useraccount;

class UseraccountList extends Base
{
    const ENTITY_CLASS = '\BO\Zmsentities\UserAccount';

    /**
     * @return UseraccountList|Useraccount[]
     */
    public function withRights($requiredRights): UseraccountList
    {
        $collection = new static();
        foreach ($this as $useraccount) {
            if ($useraccount->hasRights($requiredRights)) {
                $collection[] = clone $useraccount;
            }
        }
        return $collection;
    }

    /**
     * @return UseraccountList|Useraccount[]
     */
    public function withoutDublicates(): UseraccountList
    {
        $collection = new self();
        foreach ($this as $useraccount) {
            if (! $collection->hasEntity($useraccount->getId())) {
                $collection->addEntity(clone $useraccount);
            }
        }
        return $collection;
    }
}
