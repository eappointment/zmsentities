<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Collection;

use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule;
use BO\Zmsentities\Helper\MaintenanceSchedule as MaintenanceScheduleHelper;
use BO\Zmsentities\Schema\Entity;
use DateTimeImmutable;

/**
 * @SuppressWarnings(ShortVariable)
 */
class MaintenanceScheduleList extends Base
{
    const ENTITY_CLASS = '\BO\Zmsentities\MaintenanceSchedule';

    /**
     * this currently sorts by using the end time to include ongoing maintenance
     */
    public function sortByScheduleEndTime(?\DateTimeInterface $nowTime = null): MaintenanceScheduleList
    {
        $nowTime = $nowTime ?? new DateTime('@' . Entity::getCurrentTimestamp());
        $comparison = self::compareTandD();
        $this->uasort(function (?MaintenanceSchedule $a, ?MaintenanceSchedule $b) use ($nowTime, $comparison) {
            $v1 = ['duration' => $a['duration']];
            $v2 = ['duration' => $b['duration']];
            $v1['time'] = MaintenanceScheduleHelper::getNextRunEnd($a, $nowTime);
            $v2['time'] = MaintenanceScheduleHelper::getNextRunEnd($b, $nowTime);
            return $comparison($v1, $v2);
        });

        return $this;
    }

    /**
     * this currently sorts by using the end time to include ongoing maintenance
     */
    public function sortByScheduleStartTime(?\DateTimeInterface $nowTime = null): MaintenanceScheduleList
    {
        $nowTime = $nowTime ?? new DateTime('@' . Entity::getCurrentTimestamp());
        $comparison = self::compareTandD();
        $this->uasort(function (?MaintenanceSchedule $a, ?MaintenanceSchedule $b) use ($nowTime, $comparison) {
            $v1 = ['duration' => $a['duration']];
            $v2 = ['duration' => $b['duration']];
            $v1['time'] = MaintenanceScheduleHelper::getNextRunTime($a, $nowTime);
            $v2['time'] = MaintenanceScheduleHelper::getNextRunTime($b, $nowTime);
            return $comparison($v1, $v2);
        });

        return $this;
    }

    protected static function compareTandD(): callable
    {
        return function (array $a, array $b) {
            if (!$a['time'] instanceof DateTimeImmutable) {
                return -1;
            }
            if (!$b['time'] instanceof DateTimeImmutable) {
                return 1;
            }

            if ((int)$a['time']->format('U') - (int)$b['time']->format('U') === 0) {
                return (int)$b['duration'] - (int)$a['duration'];
            }
            return (int)$a['time']->format('U') - (int)$b['time']->format('U');
        };
    }

    public function getActiveEntry(): ?MaintenanceSchedule
    {
        /** @var MaintenanceSchedule $entry */
        foreach ($this as $entry) {
            if ($entry->isActive()) {
                return $entry;
            }
        }

        return null;
    }
}
