<?php
namespace BO\Zmsentities\Collection;

use BO\Zmsentities\Cluster;
use BO\Zmsentities\Department;
use BO\Zmsentities\Helper\Property;
use BO\Zmsentities\Scope;
use BO\Zmsentities\Useraccount;

class DepartmentList extends Base implements JsonUnindexed
{
    const ENTITY_CLASS = '\BO\Zmsentities\Department';

    /**
     * @return DepartmentList|Department[]
     */
    public function withOutClusterDuplicates(): DepartmentList
    {
        $departmentList = new self();
        foreach ($this as $department) {
            $entity = new Department($department);
            $departmentList->addEntity($entity->withOutClusterDuplicates());
        }
        return $departmentList;
    }

    /**
     * @return ScopeList|Scope[]
     */
    public function getUniqueScopeList(): ScopeList
    {
        $scopeList = new ScopeList();
        $clusterList = $this->getUniqueClusterList();
        foreach ($this as $department) {
            $entity = new Department($department);
            foreach ($entity->scopes as $scope) {
                $scope = new Scope($scope);
                $scopeList->addEntity($scope);
            }
        }
        foreach ($clusterList as $cluster) {
            foreach ($cluster->scopes as $scope) {
                $scope = new Scope($scope);
                $scopeList->addEntity($scope);
            }
        }
        return $scopeList->withUniqueScopes();
    }

    /**
     * @return ClusterList|Cluster[]
     */
    public function getUniqueClusterList():ClusterList
    {
        $clusterList = new ClusterList();
        foreach ($this as $department) {
            if (Property::__keyExists('clusters', $department)) {
                foreach ($department['clusters'] as $cluster) {
                    $entity = new Cluster($cluster);
                    $clusterList->addEntity($entity);
                }
            }
        }
        return $clusterList->withUniqueClusters();
    }

    /**
     * @return DepartmentList|Department[]
     */
    public function withAccess(Useraccount $useraccount): DepartmentList
    {
        $list = new static();
        /** @var Department $department */
        foreach ($this as $department) {
            if ($department->hasAccess($useraccount)) {
                $list->addEntity(clone $department);
            }
        }
        return $list;
    }

    /**
     * @return array
     */
    public function withGroupedByName(): array
    {
        $list = [];
        /** @var Department $department */
        foreach ($this as $department) {
            $list[$department->name][] = clone $department;
        }
        return $list;
    }

    /**
     * @param string $name
     * @return DepartmentList
     */
    public function withName(string $name = 'Bürgeramt'): DepartmentList
    {
        $list = new static();
        /** @var Department $department */
        foreach ($this as $department) {
            if ($department->name === $name) {
                $list->addEntity(clone $department);
            }
        }
        return $list;
    }

    /**
     * @param string $name
     * @return DepartmentList
     */
    public function withoutName(string $name = 'Bürgeramt'): DepartmentList
    {
        $list = new static();
        /** @var Department $department */
        foreach ($this as $department) {
            if ($department->name !== $name) {
                $list->addEntity(clone $department);
            }
        }
        return $list;
    }

    /**
     * @return DepartmentList|Department[]
     */
    public function withMatchingScopes(ScopeList $scopeList): DepartmentList
    {
        $list = new static();
        foreach ($this as $department) {
            $entity = clone $department;
            $entity->scopes = new ScopeList();
            $departmentScopeList = $department->getScopeList()->withUniqueScopes();
            foreach ($scopeList as $scope) {
                if ($departmentScopeList->hasEntity($scope->id)) {
                    $entity->scopes->addEntity($scope);
                }
            }
            if ($entity->scopes->count()) {
                $list->addEntity($entity);
            }
        }
        return $list;
    }

    /**
     * @return DepartmentList|Department[]
     */
    public function sortByName(): DepartmentList
    {
        parent::sortByName();
        foreach ($this as $department) {
            if (isset($department->clusters) && $department->clusters instanceof ClusterList) {
                $department->clusters->sortByName();
            }
            if (isset($department->scopes) && $department->scopes instanceof ScopeList) {
                $department->scopes->sortByName();
            }
        }
        return $this;
    }
}
