<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Collection;

class AccessStatsList extends Base
{
    const ENTITY_CLASS = '\BO\Zmsentities\AccessStats';

    public function getCountedGroupBy($attribute): array
    {
        $sum = [];

        foreach ($this as $entry) {
            if ($entry[$attribute]) {
                $sum[$entry[$attribute]] = isset($sum[$entry[$attribute]]) ? $sum[$entry[$attribute]] + 1 : 1;
            }
        }

        return $sum;
    }
}
