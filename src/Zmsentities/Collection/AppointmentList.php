<?php
namespace BO\Zmsentities\Collection;

use \BO\Zmsentities\Appointment;
use BO\Zmsentities\Process;

class AppointmentList extends Base
{
    const ENTITY_CLASS = '\BO\Zmsentities\Appointment';

    public function getByDate($date)
    {
        foreach ($this as $item) {
            if ($item['date'] == $date) {
                return $item instanceof Appointment ? $item : new Appointment($item);
            }
        }
        return false;
    }

    public function hasDateScope($date, $scopeId)
    {
        $item = $this->getByDate($date);
        if ($item && $item->toProperty()->scope->id->get() == $scopeId) {
            return true;
        }
        return false;
    }

    public function hasAppointment(\BO\Zmsentities\Appointment $appointment)
    {
        foreach ($this as $appointmentItem) {
            if ($appointmentItem->isMatching($appointment)) {
                return true;
            }
        }
        return false;
    }

    public function getCalculatedSlotCount()
    {
        $slotCount = 0;
        foreach ($this as $appointmentItem) {
            $slotCount += $appointmentItem->getSlotCount();
        }
        return $slotCount;
    }

    /**
     * @return ProcessList
     */
    public function toProcessList(): ProcessList
    {
        $processList = new ProcessList();
        foreach ($this as $appointment) {
            $process = new Process();
            $process->addAppointment($appointment);
            $process->scope = $appointment->scope;
            $processList->addEntity($process);
        }
        return $processList;
    }
}
