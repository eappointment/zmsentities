<?php

declare(strict_types=1);

namespace BO\Zmsentities\Exception;

class SchemaCreation extends \Exception
{
    protected $code = 500;
}
