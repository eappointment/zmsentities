<?php

namespace BO\Zmsentities\Helper;

use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Day;
use BO\Zmsentities\Process;

use DateTimeInterface;
use Exception;

/**
 * @suppressWarnings(complexity)
 */
class TimeRestriction
{
    public const NA = 'nicht buchbar';
    public const RESTRICTED = 'ausgenommen';
    public const FULL = 'ausgebucht';

    protected array $dayTimesRanges = [
        'früh' => [
            'begin' => 1,
            'end' => 7,
            'hide' => true,
        ],
        'vormittags' => [
            'begin' => 7,
            'end' => 13,
            'hide' => false,
        ],
        'nachmittags' => [
            'begin' => 13,
            'end' => 19,
            'hide' => false,
        ],
        'abends' => [
            'begin' => 19,
            'end' => 24,
            'hide' => true,
        ],
    ];

    protected array $daysOfWeek = [
        'So' => 'Sonntag',
        'Mo' => 'Montag',
        'Di' => 'Dienstag',
        'Mi' => 'Mittwoch',
        'Do' => 'Donnerstag',
        'Fr' => 'Freitag',
        'Sa' => 'Samstag',
    ];

    private string $timerestriction;

    public function __construct(string $timerestriction = '')
    {
        $this->timerestriction = $timerestriction;
        return $this;
    }

    /**
     * @param ProcessList $freeProcessList
     * @return array
     */
    public function getAppointmentListFromDay(ProcessList $freeProcessList): array
    {
        $timeList = [];
        foreach ($freeProcessList as $process) {
            $process = new Process($process);
            $appointment = $process->getFirstAppointment();
            $hour = $appointment->toDateTime()->format('H') . ':00';
            $appointmentTime = $appointment->toDateTime()->format('H:i');
            $providerId = $process->scope->getProviderId();
            if (! isset($timeList[$providerId][$hour]['times'][$appointmentTime])) {
                $timeList[$providerId][$hour]['times'][$appointmentTime] = 0;
            }
            //$timeList[$providerId][$hour]['times'][$appointmentTime] += $process->getAppointments()->count();
            $timeList[$providerId][$hour]['appointments'][$appointmentTime] = $appointment;
        }
        return $timeList;
    }

    /**
     * @param DateTimeInterface $appointmentDate
     * @param ProcessList $freeProcessList
     * @return array
     * @throws Exception
     */
    public function getTimesOfDay(DateTimeInterface $appointmentDate, ProcessList $freeProcessList): array
    {
        $resort = [];
        $appointmentList = $this->getAppointmentListFromDay($freeProcessList);
        foreach ($appointmentList as $providerId => $appointmentHours) {
            $match = $this->getMatchedWithAppointmentList($appointmentDate, $appointmentHours, $providerId);
            foreach ($match as $daypart => $provider) {
                $resort[$daypart]['daypart'] = $provider['daypart'];
                unset($provider['daypart']);
                $resort[$daypart]['providerList'][$providerId] = $provider;
            }
        }
        foreach ($resort as $daypart => $data) {
            $resort[$daypart]['free'] = $this->sumFreeAppointments($data['providerList']);
        }
        return $resort;
    }

    /**
     * Returns a formated array of the timerestriction for the output in templates
     *
     * @return array
     * @throws Exception
     */
    public function getTimeRangeDetails(): array
    {
        $result = [];
        if (!$this->isActive()) {
            $result[] =  [
                'id' => 'all',
                'weekday' => 'Alle Tage',
                'daypart' => 'Alle Tageszeiten',
                'timerange' => 'Alle Zeiträume'
            ];
        } else {
            foreach ($this->getRestrictionParts() as $restriction) {
                $starttime = str_pad($restriction['start'], 2, '0', STR_PAD_LEFT) . ':00';
                $daypart = $this->getDayPartForHour($restriction['start']);
                $endtime = str_pad(($restriction['end'] - 1), 2, '0', STR_PAD_LEFT) . ':59 Uhr';
                $result[] = [
                    'id' => $restriction['day'] . $restriction['start'] . $restriction['end'],
                    'weekday' => $this->daysOfWeek[$restriction['day']],
                    'daypart' => $daypart['label'],
                    'timerange' => $starttime . '-' . $endtime,
                ];
            }
        }
        return $result;
    }

    /**
     * Get the components of the restriction
     *
     * @return array
     */
    public function getRestrictionParts(): array
    {
        if (!$this->isActive()) {
            $timerestrictionList = $this->getAllCombinations();
        } else {
            $timerestrictionList = explode(',', $this->timerestriction);
        }
        $parts = [];
        foreach ($timerestrictionList as $part) {
            $restriction = array_combine(['day', 'start', 'end'], explode('-', $part));
            $parts[] = $restriction;
        }
        return $parts;
    }

    /**
     * Check if a restriction is active
     *
     * @return Bool
     */
    public function isActive(): bool
    {
        return ($this->timerestriction != '');
    }

    public function sumFreeAppointments($times)
    {
        $sum = 0;
        if (count($times)) {
            foreach ($times as $time) {
                if ($time['free']) {
                    $sum += $time['free'];
                }
            }
        }
        return $sum;
    }

    /**
     * @return array
     */
    public function getDaysOfWeek(): array
    {
        return array_diff_key($this->daysOfWeek, ['So' => '']);
    }

    /**
     * @return array
     */
    public function getActiveDayParts(): array
    {
        return array_filter($this->dayTimesRanges, function ($data) {
            return !$data['hide'];
        });
    }



    /**
     * Returns every possible time restriction combinations
     * @return array
     */
    public function getAllCombinations(): array
    {
        $combines = [];
        foreach (array_keys($this->daysOfWeek) as $shortDayName) {
            foreach ($this->dayTimesRanges as $timeRange) {
                $combines[] = $shortDayName . '-' . $timeRange['begin'] . '-' . $timeRange['end'];
            }
        }
        return $combines;
    }



    /**
     * Get the identifier for a day according a given date
     *
     * @param Int $timestamp UNIX Timestamp
     *
     */
    public function getDayOfWeekIndex(int $timestamp): string
    {
        $dow = date('w', $timestamp);
        $daysOfWeek = array_keys($this->daysOfWeek);
        return $daysOfWeek[$dow];
    }

    /**
     * Check, if day and time is allowed in the given time restrictions
     *
     * @param Mixed $date UNIX Timestamp or a strtotime() compatible string
     *
     * @return Bool
     */
    public function isAllowed(mixed $date): bool
    {
        $timestamp = (is_int($date)) ? $date : strtotime($date);
        $hour = date('H', $timestamp);
        $dow = $this->getDayOfWeekIndex($timestamp);
        foreach ($this->getRestrictionParts() as $restriction) {
            if ($dow == $restriction['day'] && $hour >= $restriction['start'] && $hour < $restriction['end']) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check, whether the datetime is on an allowed day (regardless the time is within restrictions of that day)
     *
     * @param DateTimeInterface $dateTime
     * @return Bool
     */
    public function isAtAllowedDay(DateTimeInterface $dateTime): bool
    {
        $dow = $this->getDayOfWeekIndex($dateTime->getTimestamp());

        return in_array($dow, array_column($this->getRestrictionParts(), 'day'));
    }

    public function isAllowedDay(Day $day): bool
    {
        return $this->isAtAllowedDay($day->toDateTime());
    }

    /**
     * Get the detail information for an hour
     * Each day has four time ranges, this
     * function gets the right time range for
     * a given hour
     *
     * @param Int $hour in 24 hours format
     *
     * @return array
     * @throws Exception
     */
    public function getDayPartForHour(int $hour): array
    {
        foreach ($this->dayTimesRanges as $label => $dayPart) {
            if (($hour >= $dayPart['begin'] && $hour < $dayPart['end'])
                || ($dayPart['end'] < $dayPart['begin'] && $hour >= $dayPart['begin'] && $hour < 24)
                || ($dayPart['end'] < $dayPart['begin'] && $hour < $dayPart['end'])
            ) {
                $dayPart['label'] = $label;
                return $dayPart;
            }
        }
        throw new Exception("Could not resolve daypart for hour $hour");
    }

    /**
     * @throws Exception
     */
    public function getMatchedWithAppointmentList(
        DateTimeInterface $appointmentDate,
        $appointmentHours,
        $providerId
    ): array {
        $matches = [];
        foreach ($this->getRestrictionParts() as $restriction) {
            $times = [];
            $times = $this->getCalculateAppointmentTimes(
                $restriction['start'],
                $restriction['end'],
                $appointmentHours,
                $appointmentDate
            );
            $daypart = $this->getDayPartForHour($times[0]['hour']);
            $matches[$daypart['label']] = [
                'range' => $times[0]['hour'] . ':00 - ' . end($times)['hour'] . ':59 Uhr',
                'times' => $times,
                'providerId' => $providerId,
                'daypart' => $daypart,
                'free' => $this->sumFreeAppointments($times),
                'appointments' => (isset($times['appointments'])) ? $times['appointments'] : [],
            ];
        }
        return $matches;
    }

    public function getCalculateAppointmentTimes(
        int $starthour,
        int $endhour,
        array $appointmentHours,
        DateTimeInterface $appointmentDate
    ): array {
        $times = [];
        if ($starthour > $endhour) {
            $endhour += 24;
        }
        for ($hourcount = $starthour; $hourcount < $endhour; $hourcount++) {
            $hour = ($hourcount == 24) ? 0 : $hourcount;
            $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);

            if (in_array($hour . ':00', array_keys($appointmentHours))
                && $this->isAllowed(strtotime($hour . ':00', $appointmentDate->getTimestamp()))
            ) {
                $times[] = array(
                    'hour' => $hour,
                    'free' => count($appointmentHours[$hour . ":00"]['appointments']),
                    'appointments' => $appointmentHours[$hour . ":00"]['appointments'],
                    'closed' => false,
                );
            } else {
                $times[] = array(
                    'hour' => $hour,
                    'free' => false,
                    'closed' => true,
                );
            }
        }
        return $times;
    }
}
