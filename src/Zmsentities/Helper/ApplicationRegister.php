<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Helper;

use BO\Zmsentities\Calldisplay;
use BO\Zmsentities\ApplicationRegister as Entity;
use BO\Zmsentities\Collection\QueueList;

class ApplicationRegister
{
    public static function getEntityByCalldisplay(Calldisplay $calldisplay): Entity
    {
        $regEntity = new Entity();
        $regEntity->id = $calldisplay->hash;
        $regEntity->type = Entity::TYPE_ZMS2_CALLDISPLAY;
        $regEntity->scopeId = Application::getScopeIdByHash($calldisplay->hash);

        if (!$regEntity->scopeId && $calldisplay->hasScopeList()) {
            $regEntity->scopeId = (int) $calldisplay->getScopeList()->getIds()[0];
        }
        if (!empty($calldisplay->status)) {
            $status = $calldisplay->status;
            $regEntity->userAgent = $status['agent'] ?? null;
            $parameters = [
                'collections' => [],
                'queue'       => (strlen($status['list'] ?? '') > 0) ? ['status' => $status['list']] : null,
                'template'    => $status['template'] ?? null,
                'qrcode'      => $status['qrcode'] ?? null,
            ];
            if ($calldisplay->hasScopeList()) {
                $parameters['collections']['scopelist'] = implode(',', $calldisplay->getScopeList()->getIds());
            }
            if ($calldisplay->hasClusterList()) {
                $parameters['collections']['clusterlist'] = implode(',', $calldisplay->getClusterList()->getIds());
            }
            $regEntity->parameters = urldecode(http_build_query($parameters));
        }

        return $regEntity;
    }

    public static function hasChangedAsRequested(Entity $entity): bool
    {
        if ($entity->requestedChange) {
            foreach ($entity->requestedChange as $key => $value) {
                if ($key === 'parameters') {
                    $parametersArray = [];
                    $requestedArray = [];
                    parse_str($entity->parameters, $parametersArray);
                    parse_str($value, $requestedArray);

                    // some default values to exclude unnecessary differences
                    $parametersArray['sid'] = $parametersArray['sid'] ?? $entity['id'] ?? '';
                    $parametersArray['qrcode'] = intval($parametersArray['qrcode'] ?? 0);
                    $parametersArray['queue'] = $parametersArray['queue'] ?? [
                        'status' => implode(',', QueueList::STATUS_CALLED)
                    ];
                    $requestedArray['sid'] = $requestedArray['sid'] ?? $entity['id'] ?? '';
                    $requestedArray['qrcode'] = intval($requestedArray['qrcode'] ?? 0);
                    $requestedArray['queue'] = $requestedArray['queue'] ?? [
                        'status' => implode(',', QueueList::STATUS_CALLED)
                    ];

                    $currentParams = self::sortLists($parametersArray);
                    $requestedParams = self::sortLists($requestedArray);

                    if ($currentParams != $requestedParams) {
                        return false;
                    }
                    continue;
                }

                if ($entity[$key] !== $value) {
                    return false;
                }
            }
        }

        return true;
    }

    protected static function sortLists(array $parameterArray): array
    {
        foreach ($parameterArray as $key => $value) {
            if (in_array(strtolower($key), ['scopelist', 'clusterlist'])) { // buttonlist must not be sorted
                $tmp = explode(',', $value);
                natsort($tmp);
                $parameterArray[$key] = implode(',', $tmp);
            }

            if (is_array($value)) {
                $parameterArray[$key] = self::sortLists($value);
            }
        }

        return $parameterArray;
    }
}
