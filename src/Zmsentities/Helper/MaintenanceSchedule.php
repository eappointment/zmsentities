<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Helper;

use BO\Zmsentities\MaintenanceSchedule as ScheduleEntity;
use Cron\CronExpression;

class MaintenanceSchedule
{
    public static function getNextRunTime(ScheduleEntity $entity, ?DateTime $nowTime = null): ?DateTime
    {
        $nowTime = $nowTime ?? new DateTime();
        if ($entity->isRepetitive()) {
            $scheduled = new CronExpression($entity->getTimeString());
            return DateTime::create($scheduled->getNextRunDate($nowTime));
        } else {
            $runDateTime = new DateTime($entity->getTimeString());
            if ($runDateTime > $nowTime) {
                return $runDateTime;
            }
        }

        return null;
    }

    public static function getNextRunEnd(ScheduleEntity $entity, ?DateTime $nowTime = null): ?DateTime
    {
        $nowTime = $nowTime ?? new DateTime('-' . ($entity->getDuration()  + 1) . ' minutes');
        $nextRunTime = self::getNextRunTime($entity, $nowTime);

        if ($nextRunTime === null) {
            return null;
        }

        return $nextRunTime->modify('+' . $entity->getDuration() . ' minutes');
    }

    public static function getReducedArray(ScheduleEntity $entity): array
    {
        $data = (array) $entity;

        if ($data['startDateTime'] instanceof \DateTimeInterface) {
            $data['startDateTime'] = $data['startDateTime']->format(DATE_ATOM);
        }

        unset($data['$schema']);
        unset($data['creatorId']);
        unset($data['creationDateTime']);
        unset($data['announcement']);
        unset($data['documentBody']);

        return $data;
    }
}
