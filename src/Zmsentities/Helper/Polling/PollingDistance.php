<?php

namespace BO\Zmsentities\Helper\Polling;

class PollingDistance
{
    /**
     * @var int $jsonCompressLevel
     */
    protected int $jsonCompressLevel = 0;

    public function __construct(array $data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }

    public function setJsonCompressLevel(int $jsonCompressLevel = 0): self
    {
        $this->jsonCompressLevel = $jsonCompressLevel;
        return $this;
    }

    public function getJsonCompressLevel(): int
    {
        return $this->jsonCompressLevel;
    }
}
