<?php

namespace BO\Zmsentities\Helper\Polling;

class PollingDistanceList extends \ArrayObject
{
    public function toGrouped($groupname = 'department'): array
    {
        $grouped = array();
        foreach ($this as $result) {
            $group = $result->$groupname;
            if (!isset($grouped[$group])) {
                $grouped[$group] = new self();
            }
            $grouped[$group][] = $result;
        }
        ksort($grouped);
        return $grouped;
    }

    public function setJsonCompressLevel(int $jsonCompressLevel = 0): void
    {
        foreach ($this as $item) {
            $item->setJsonCompressLevel($jsonCompressLevel);
        }
    }
}
