<?php

declare(strict_types=1);

namespace BO\Zmsentities\Helper;

use BO\Zmsentities\Collection\Base;
use BO\Zmsentities\Exception\SchemaCreation;
use BO\Zmsentities\Schema\Entity;
use BO\Zmsentities\Schema\Factory;

class Collection
{
    /**
     * creating the corresponding list when the schema info is consistent
     * @param array $data
     * @return Base|null
     */
    public static function createCollectionFromArray(array $data): ?Base
    {
        /** @var Base $list */
        $list = null;
        foreach ($data as $entityData) {
            try {
                $entity = $entityData instanceof Entity ? $entityData : Factory::createEntity($entityData);
            } catch (SchemaCreation $exception) {
                continue;
            }

            if ($list && trim($list::ENTITY_CLASS, '\\') != get_class($entity)) {
                return null; //inconsistent data
            } elseif (!$list = self::getEntityRelatedList($entity)) {
                return null;
            }

            $list->addEntity($entity);
        }

        return $list;
    }

    public static function getEntityRelatedList(Entity $entity)
    {
        $classParts = explode('\\', get_class($entity));
        $className  = array_pop($classParts) . 'List';

        $classParts[] = 'Collection';
        $listName  = implode('\\', $classParts) . '\\' . $className;

        try {
            $list = new $listName();
        } catch (\Error $error) {
            return null;
        }

        return $list;
    }
}
