<?php

declare(strict_types=1);

namespace BO\Zmsentities;

use BO\Zmsentities\Exception\SchemaValidation;

/**
 * @property string status
 * @property string scopeIds
 * @property bool startBeforeAvailability
 * @property int startBeforeMinutes
 * @property int tooEarlyMinutes
 * @property int toleranceMinutes
 * @property bool activeAfterClosing
 * @property string msgAlternative
 * @property string msgLate
 * @property string msgEarly
 * @property string msgWrongDay
 * @property string msgWrongCounter
 * @property string msgSuccess
 * @property string msgMaintenance
 * @property null|string lastChange
 */
class CheckInConfig extends Schema\Entity
{
    public const PRIMARY = 'id';

    public const STATUS_ON = 'on';
    public const STATUS_OFF = 'off';
    public const STATUS_INFO = 'info';
    public const STATUS_MAINTENANCE = 'maintenance';

    public static $schema = "checkInConfig.json";

    public function getEntityName()
    {
        return 'checkInConfig';
    }

    public function getDefaults(): array
    {
        return [
            'id' => substr(md5(random_bytes(20)), 10, 12),
            'status' => 'off',
            'scopeIds' => '',
            'startBeforeAvailability' => false,
            'startBeforeMinutes' => 15,
            'tooEarlyMinutes' => 30,
            'toleranceMinutes' => 0,
            'activeAfterClosing' => false,
            'languages' => '',
            'msgAlternative' => '{}',
            'msgLate' => '{}',
            'msgEarly' => '{}',
            'msgWrongDay' => '{}',
            'msgWrongCounter' => '{}',
            'msgSuccess' => '{}',
            'msgMaintenance' => '{}',
            'lastChange' => null,
        ];
    }

    /**
     * @SuppressWarnings(Complexity)
     * {@inheritDoc}
     */
    public function addData($mergeData): Schema\Entity
    {
        if (is_string($mergeData['tooEarlyMinutes'] ?? null) && is_numeric($mergeData['tooEarlyMinutes'])) {
            $mergeData['tooEarlyMinutes'] = (int) $mergeData['tooEarlyMinutes'];
        }
        if (is_string($mergeData['toleranceMinutes'] ?? null) && is_numeric($mergeData['toleranceMinutes'])) {
            $mergeData['toleranceMinutes'] = (int) $mergeData['toleranceMinutes'];
        }
        //
        if (isset($mergeData['msgAlternative']) && is_string($mergeData['msgAlternative'])
            && (!isset($mergeData['msgMaintenance']) || strlen($mergeData['msgMaintenance']) < 3)
        ) {
            $mergeData['msgMaintenance'] = $mergeData['msgAlternative']; //use msgAlternative for both if not given else
        }

        return parent::addData($mergeData);
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ON;
    }

    public function isOperating(): bool
    {
        return $this->status !== self::STATUS_OFF;
    }

    public function isScopeContained(int $scopeId): bool
    {
        return in_array($scopeId, $this->getScopeIds());
    }

    public function setScopeIds(array $scopeIds): static
    {
        $this['scopeIds'] = '';

        if (count($scopeIds)) {
            $this['scopeIds'] = ':' . implode(':', $scopeIds) . ':';
        }

        return $this;
    }

    public function getScopeIds(): array
    {
        $scopeIds = explode(':', trim($this['scopeIds'] ?? '', ':'));
        array_walk(
            $scopeIds,
            function (&$value) {
                $value = intval($value);
            }
        );

        return $scopeIds;
    }

    /**
     * @param string $localeCode ( ISO 15897 POSIX locale preferred )
     * @return array
     */
    public function getTranslations(string $localeCode = 'de_DE'): array
    {
        $localeCode   = str_replace('-', '_', $localeCode); // IETF BCP 47 language tag --> ISO 15897 POSIX locale
        $translations = [];

        foreach ($this as $key => $value) {
            if (str_starts_with($key, 'msg')) {
                $keyTranslations = json_decode($value, true) ?? [];
                if (!isset($keyTranslations[$localeCode])) {
                    continue;
                }
                $translations[$key] = $keyTranslations[$localeCode];
            }
        }

        return $translations;
    }

    /**
     * Check if the given data validates against the given jsonSchema
     *
     * @param string $locale
     * @param int    $resolveLevel
     *
     * @return Boolean
     * @throws SchemaValidation
     * @throws \Exception
     */
    public function testValid($locale = 'de_DE', $resolveLevel = 0, string $testedPath = ''): bool
    {
        $isValid = parent::testValid($locale, $resolveLevel);

        $exception = new Exception\SchemaValidation();
        $exception->setSchemaName($this->getEntityName());
        $languages = [];

        foreach ($languages as $languageCode) {
            if ($languageCode && false) { //todo
                $isValid = false;
                $exception->data['msgAlternative']['messages'] = ['Der Startzeitpunkt muss in der Zukunft liegen'];
            }
        }

        if (!$isValid) {
            throw $exception;
        }


        return $isValid;
    }
}
