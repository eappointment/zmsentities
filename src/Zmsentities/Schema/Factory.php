<?php


namespace BO\Zmsentities\Schema;

use BO\Zmsentities\Exception\SchemaCreation;
use BO\Zmsentities\Exception\SchemaMissingKey;
use BO\Zmsentities\Helper\Property;

/**
 * @SuppressWarnings(NumberOfChildren)
 */
class Factory
{
    /**
     * @var array $data unserialized entity
     */
    protected $data = null;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * this returns an enriched factory object
     *
     * @param array $data
     * @return self
     */
    public static function create($data)
    {
        return new self($data);
    }

    /**
     * @param array $data
     * @return Entity
     * @throws SchemaCreation (SchemaCreation | SchemaMissingKey)
     */
    public static function createEntity(array $data): Entity
    {
        $factory = self::create($data);
        try {
            return $factory->getEntity();
        } catch (\Error $error) {
            if (strpos($error->getMessage(), 'not found')) {
                throw new SchemaCreation('Entity ' . $factory->getEntityName() . ' was not found.');
            }
            throw $error;
        }
    }

    /**
     * Detect type of entity and return initialized object
     *
     * @return Entity
     */
    public function getEntity()
    {
        $entityName = $this->getEntityName();
        $class = "\\BO\\Zmsentities\\$entityName";
        return new $class(new UnflattedArray($this->data));
    }

    /**
     * Parse schema and return Entity name
     *
     * @return String
     */
    public function getEntityName()
    {
        if (!Property::__keyExists('$schema', $this->data)) {
            throw new SchemaMissingKey('Missing $schema-key on given data.');
        }
        $schema = $this->data['$schema'];
        $entityName = preg_replace('#^.*/([^/]+)\.json#', '$1', $schema);
        return ucfirst($entityName);
    }
}
