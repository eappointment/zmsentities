<?php

namespace BO\Zmsentities\Schema;

use ArrayObject;
use BO\Zmsentities\Helper\DateTime;
use InvalidArgumentException;

class UnflattedArray
{
    /** @var array|null */
    protected $value = null;

    /**
     * @param array|ArrayObject $value
     */
    public function __construct($value)
    {
        $this->value = (array) $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    /**
      * split fields
      * If a key to a field has two underscores "__" it should go into a subarray
      * ATTENTION: performance critical function, keep highly optimized!
      *
      * @return array
      */
    public function getUnflattenedArray()
    {
        $hash = $this->value;
        foreach ($hash as $key => $value) {
            if (false !== strpos($key, '__')) {
                $currentLevel =& $hash;
                unset($hash[$key]);
                foreach (explode('__', $key) as $currentKey) {
                    if (!isset($currentLevel[$currentKey])) {
                        $currentLevel[$currentKey] = [];
                    }
                    $currentLevel =& $currentLevel[$currentKey];
                }
                $currentLevel = $value;
            }
        }
        $this->value = $hash;
        return (array)$hash;
    }

    /**
     * @param mixed $key
     * @return void
     * @throws InvalidArgumentException
     */
    public static function testValidArrayKey($key): void
    {
        if (!is_string($key) && !is_int($key)) {
            throw new InvalidArgumentException('the array key must be a string or integer');
        }
    }

    /**
     * @param mixed $key
     * @param mixed $value
     * @return void
     */
    public function offsetSet($key, $value): void
    {
        self::testValidArrayKey($key);

        $this->value[$key] = $value;
    }

    /**
     * @param string|int $key
     * @return int (null when not in input)
     */
    public function offsetStrlen($key): ?int
    {
        self::testValidArrayKey($key);

        if (array_key_exists($key, $this->value)
            && is_string($this->value[$key])
        ) {
            return strlen($this->value[$key]);
        }

        return null;
    }

    /**
     * @param mixed $key
     * @param UnflattedArray $target
     * @param bool $castType (use type casting when true or don't copy when false and type is not as expected)
     * @return void
     */
    public function copyBoolean($key, UnflattedArray $target, bool $castType = true): void
    {
        if (!array_key_exists($key, $this->value)) {
            return;
        }
        if (!$castType && !is_bool($this->value[$key])) {
            return;
        }

        $target->offsetSet($key, (bool) $this->value[$key]);
    }

    /**
     * @param mixed $key
     * @param UnflattedArray $target
     * @param bool $castType (use type casting when true or don't copy when false and type is not as expected)
     * @return void
     */
    public function copyInteger($key, UnflattedArray $target, bool $castType = true): void
    {
        if (!array_key_exists($key, $this->value)) {
            return;
        }
        if (!$castType && !is_int($this->value[$key])) {
            return;
        }

        $target->offsetSet($key, (int) $this->value[$key]);
    }

    /**
     * @param mixed $key
     * @param UnflattedArray $target
     * @param bool $castType (use type casting when true or don't copy when false and type is not as expected)
     * @return void
     */
    public function copyString($key, UnflattedArray $target, bool $castType = true): void
    {
        if (!array_key_exists($key, $this->value)) {
            return;
        }
        if (!$castType && !is_string($this->value[$key])) {
            return;
        }

        $target->offsetSet($key, (string) $this->value[$key]);
    }

    /**
     * default: copies arrays only, while ArrayObject is converted to array
     *
     * @param mixed $key
     * @param UnflattedArray $target
     * @param bool $castType (use type casting when true or don't copy when false and type is not as expected)
     * @return void
     */
    public function copyDateTime($key, UnflattedArray $target, bool $castType = true): void
    {
        if (!array_key_exists($key, $this->value)) {
            return;
        }
        if ((!$castType && !$this->value[$key] instanceof \DateTimeInterface)
            || ($castType && !$this->value[$key] instanceof \DateTimeInterface
                && (!is_string($this->value[$key]) || strlen($this->value[$key]) === 0))
        ) {
            return;
        }

        $target->offsetSet($key, DateTime::create($this->value[$key]));
    }
}
