<?php

namespace BO\Zmsentities;

class Status extends Schema\Entity
{
    const PRIMARY = 'version';
    public static $schema = "status.json";

    public function getDefaults()
    {
        return [
            'database' => array (
                'nodeConnections' => 0.0,
                'clusterStatus' => 'OFF',
                'logbin' => 'OFF',
            ),
            'processes' => array (
                'blocked' => 0,
                'confirmed' => 0,
                'deleted' => 0,
                'missed' => 0,
                'reserved' => 0,
                'lastInsert' => 0,
            ),
            'mail' => array (
                'queueCount' => 0,
                'oldestSeconds' => 0,
                'newestSeconds' => 0,
            ),
            'notification' => array (
                'queueCount' => 0,
                'oldestSeconds' => 0,
                'newestSeconds' => 0,
            ),
        ];
    }

    public function getFilteredByCategory(string $category)
    {
        $entity = clone $this;
        $filteredEntity = array_filter($entity->getArrayCopy(), function ($key) use ($category) {
            return $key === $category;
        }, ARRAY_FILTER_USE_KEY);
        $schema = array(
            '$schema' => Schema\Schema::$baseUrl . $entity->getEntityName() . '.json'
        );
        return array_merge($schema, $filteredEntity);
    }
}
