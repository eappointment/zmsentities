<?php

declare(strict_types=1);

namespace BO\Zmsentities;

use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\Schema\Entity;

/**
 * @property string status
 * @property string refersTo
 * @property int referenceId
 * @property int scopeId
 * @property int checkInTS
 * @property int scheduledTS
 * @property string checkInSource
 * @property null|DateTime checkInDateTime
 * @property DateTime scheduledDateTime
 * @property null|string lastChange
 */
class CheckInTask extends Entity
{
    public const PRIMARY = 'id';

    public const STATUS_OPEN = 'open';
    public const STATUS_LATE = 'late';
    public const STATUS_EARLY = 'early';
    public const STATUS_ARRIVED = 'arrived';
    public const STATUS_NO_SHOW = 'noShow';
    public const STATUS_NA = 'N/A';

    public const SOURCE_COUNTER_APP = 'localApp';
    public const SOURCE_COUNTER_MAN = 'manual';
    public const SOURCE_ONLINE = 'online';


    public static $schema = "checkInTask.json";

    public function getEntityName()
    {
        return 'checkInTask';
    }

    public function getDefaults(): array
    {
        return [
            'status' => 'open',
            'refersTo' => 'Process',
            'referenceId' => null,
            'scopeId' => null,
            'checkInTS' => 0,
            'scheduledTS' => 0,
            'checkInSource' => '',
            'checkInDateTime' => null,
            'scheduledDateTime' => null,
            'lastChange' => null,
        ];
    }

    public function addData($mergeData): static
    {
        if (is_string($mergeData['checkInTS'] ?? null) && is_numeric($mergeData['checkInTS'])) {
            $mergeData['checkInTS'] = (int) $mergeData['checkInTS'];
        }
        if (is_string($mergeData['scheduledTS'] ?? null) && is_numeric($mergeData['scheduledTS'])) {
            $mergeData['scheduledTS'] = (int) $mergeData['scheduledTS'];
        }
        if (isset($mergeData['checkInTS'])) {
            $mergeData['checkInDateTime'] = (new DateTime())->setTimestamp($mergeData['checkInTS']);
        }
        if (isset($mergeData['scheduledTS'])) {
            $mergeData['scheduledDateTime'] = (new DateTime())->setTimestamp($mergeData['scheduledTS']);
        }

        return parent::addData($mergeData);
    }
}
