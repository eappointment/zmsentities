<?php

namespace BO\Zmsentities;

use BO\Zmsentities\Collection\ProviderList;
use BO\Zmsentities\Collection\RequestList;
use BO\Zmsentities\Collection\RequestRelationList;
use BO\Zmsentities\Exception\SchemaValidation;
use BO\Zmsentities\Schema\Schema;
use BO\Zmsentities\Schema\Entity;

/**
 * @property string $source
 * @property Contact $contact
 * @property ProviderList|Provider[] $providers
 * @property RequestList|Request[] $requests
 * @property RequestRelationList|RequestRelation[] $requestrelation
 * @property string $label
 * @property bool|int $editable
 */
class Source extends Entity
{
    const PRIMARY = 'source';

    public static $schema = 'source.json';

    public function getDefaults()
    {
        return [
            'source' => '',
            'contact' => new Contact([
                'name' => '',
                'email' => ''
            ]),
            'providers' => new ProviderList(),
            'requests' => new RequestList(),
            'requestrelation' => new RequestRelationList(),
            'label' => '',
            'editable' => false
        ];
    }

    public function getSource()
    {
        return $this->toProperty()->source->get();
    }

    public function getLabel()
    {
        return $this->toProperty()->label->get();
    }

    public function getContact()
    {
        return $this->toProperty()->contact->get();
    }

    /**
     * @return ProviderList|Provider[]
     */
    public function getProviderList()
    {
        $providerList = new Collection\ProviderList();
        foreach ($this->toProperty()->providers->get() as $provider) {
            if (! $provider instanceof Provider) {
                $provider = new Provider($provider);
            }
            $providerList->addEntity($provider);
        }
        return $providerList;
    }

    /**
     * @return RequestList|Request[]
     */
    public function getRequestList()
    {
        $requestList = new Collection\RequestList();
        foreach ($this->toProperty()->requests->get() as $request) {
            if (! $request instanceof Request) {
                $request = new Request($request);
            }
            $requestList->addEntity($request);
        }
        return $requestList;
    }

    public function hasProvider($providerIdCsv)
    {
        $providerIds = explode(',', $providerIdCsv);
        foreach ($providerIds as $providerId) {
            if (! in_array($providerId, $this->getProviderList()->getIds())) {
                return false;
            }
        }
        return true;
    }

    public function getRequestRelationList()
    {
        $requestRelationList = new \BO\Zmsentities\Collection\RequestRelationList();
        if (isset($this['requestrelation'])) {
            foreach ($this['requestrelation'] as $entity) {
                if (! $entity instanceof RequestRelation) {
                    $entity = new RequestRelation($entity);
                }
                $requestRelationList->addEntity($entity);
            }
        }
        return $requestRelationList;
    }

    public function hasRequest($requestIdCsv)
    {
        $requestIds = explode(',', $requestIdCsv);
        foreach ($requestIds as $requestId) {
            if (! in_array($requestId, $this->getRequestList()->getIds())) {
                return false;
            }
        }
        return true;
    }

    public function testValid($locale = 'de_DE', $resolveLevel = 0, string $testedPath = '')
    {
        if ($resolveLevel === 0) {
            return parent::testValid($locale, $resolveLevel, $testedPath);
        }

        $errorData  = [];
        $testEntity = clone $this;
        $testEntity->requests = [];
        $testEntity->providers = [];

        try {
            $testEntity->testValid($locale, $resolveLevel - 1, $testedPath);
        } catch (SchemaValidation $exception) {
            $errorData = $exception->data;
        }

        if ($resolveLevel == 1) {
            try {
                $this->testAttribute('requests', $locale);
            } catch (SchemaValidation $exception) {
                unset($exception->data['source']);
                $errorData = array_merge($errorData, $exception->data, ['requests' => $exception->data]);
            }

            try {
                $this->testAttribute('providers', $locale);
            } catch (SchemaValidation $exception) {
                unset($exception->data['source']);
                $errorData = array_merge($errorData, $exception->data, ['providers' => $exception->data]);
            }
        }

        if (!empty($errorData)) {
            $exception = new SchemaValidation('validation failed');
            $exception->data = $errorData;

            throw $exception;
        }

        return true;
    }

    public function isEditable()
    {
        return ($this->toProperty()->editable->get()) ? true : false;
    }

    public function isCompleteAndEditable()
    {
        return ($this->isEditable() && 0 < $this->getProviderList()->count() && $this->getRequestList()->count());
    }

    public function withCleanedUpFormData()
    {
        $entity = parent::withCleanedUpFormData();
        $providerList = $entity->getProviderList();
        $requestList = $entity->getRequestList();
        $entity->providers = $providerList->withDataAsObject();
        $entity->requests = $requestList->withDataAsObject();
        return $entity;
    }

    public function jsonSerialize()
    {
        $data = array(
            '$schema' => Schema::$baseUrl . $this->getEntityName() . '.json'
        );
        $schema = new Schema(array_merge($data, $this->getArrayCopy()));
        $schema->setDefaults($this->getDefaults());
        $schema->setJsonCompressLevel($this->jsonCompressLevel);

        return $schema->toJsonObject(true);
    }
}
