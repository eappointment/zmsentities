<?php

namespace BO\Zmsentities;

class Apiclient extends Schema\Entity
{
    const PRIMARY = 'clientKey';

    public const ACCESS_LEVEL_PUBLIC    = 'public';
    public const ACCESS_LEVEL_BLOCKED    = 'blocked';

    public const ACCESS_LEVEL_INTERN    = 'intern';

    public const ACCESS_LEVEL_CALLCENTER    = 'callcenter';

    public static $schema = "apiclient.json";

    public function getDefaults()
    {
        return [
            'shortname' => 'default',
            'permission' => new Permission(),
        ];
    }

    public function getPermission()
    {
        if (!$this['permission'] instanceof Permission) {
            $this['permission'] = new Permission($this['permission']);
        }
        return $this['permission'];
    }
}
