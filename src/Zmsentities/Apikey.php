<?php

namespace BO\Zmsentities;

use DateTimeImmutable;
use DateTimeInterface;

class Apikey extends Schema\Entity
{
    const PRIMARY = 'key';

    public static $schema = "apikey.json";

    public function getDefaults(): array
    {
        return [
            'apiclient' => new Apiclient(),
            'locked' => 0,
            'createIP' => '',
            'ts' => self::getCurrentTimestamp(),
        ];
    }

    public function setApiClient(Apiclient $apiClient): Apikey
    {
        $this['apiclient'] = $apiClient;
        return $this;
    }

    public function getApiClient(): Apiclient
    {
        return $this['apiclient'];
    }

    /**
     * @param String $string usually the captcha text or a token
     * @param String $secret use a secret to avoid crafted api keys
     */
    public function getHash(string $string, string $secret = '4Td8x5Qn5hjW3uSc6MWWVQPwrw6b74fL'): string
    {
        $hash = hash_hmac('sha256', $string, $secret);
        $hash = base64_encode(hex2bin($hash));
        $hash = preg_replace('/[^a-zA-Z0-9]/', '', $hash);
        return substr($hash, 0, 40);
    }

    /**
     * @param String $text usually the captcha text
     * @param String $hash usually the apikey
     * @param String $secret has to be same secret used by self::getHashFromCaptcha()
     */
    public function isVerifiedHash(
        string $text,
        string $hash,
        string $secret = '4Td8x5Qn5hjW3uSc6MWWVQPwrw6b74fL'
    ): bool {
        $generatedHash = $this->getHash($text, $secret);
        return hash_equals($generatedHash, $hash);
    }

    public function withCaptchaData($base64Image): Apikey
    {
        $this->captcha = new Mimepart([
            'content' => $base64Image,
            'mime' => 'image/jpeg;base64',
            'base64' => true
        ]);
        return $this;
    }

    /**
     * gets a DateTime object from the calculation of the apikey creation timestamp
     * and the apiclient secondsToInvalidation property
     *
     * @return DateTimeInterface
     */
    public function getExpiredDateTime(): DateTimeInterface
    {
        $expireTimstamp = $this->ts + $this->getApiClient()->secondsToInvalidation;
        return (new DateTimeImmutable())->setTimestamp($expireTimstamp);
    }

    /**
     * checks if the expiration date is less than the current time and returns true or false
     *
     * @param DateTimeInterface $now
     *
     * @return Bool
     */
    public function isExpired(DateTimeInterface $now): Bool
    {
        return ($now > $this->getExpiredDateTime());
    }

    /**
     * checks if apikey is locked
     *      *
     * @return Bool
     */
    public function isLocked(): Bool
    {
        return ($this->locked === 1 || $this->locked == "1");
    }

    public function getQuotaPositionByRoute($route)
    {
        return (isset($this->quota) && is_array($this->quota)) ?
            array_search($route, array_column($this->quota, 'route'))
            : false;
    }

    public function addQuota($route, $period): Apikey
    {
        $this->quota[] = [
            'route' => $route,
            'period' => $period,
            'requests' => 1
        ];
        return $this;
    }

    public function updateQuota($position): Apikey
    {
        $this->quota[$position]['requests']++;
        return $this;
    }
}
