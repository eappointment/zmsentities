<?php

namespace BO\Zmsentities;

/**
 * permissions an useraccount can get to grant protected access to the API
 */
class Permission extends Schema\Entity
{
    public const RIGHT_PRIVILEGED = 'superuser';
    public const RIGHT_ACCESSROLES = 'accessroles';
    public const RIGHT_SYSTEMCONFIG = 'systemconfig';
    public const RIGHT_MAINTENANCE = 'maintenance';
    public const RIGHT_USERACCOUNT = 'useraccount';
    public const RIGHT_APPLICATION = 'application';
    public const RIGHT_SCOPE = 'scope';
    public const RIGHT_ORGANISATION = 'organisation';
    public const RIGHT_DEPARTMENT = 'department';
    public const RIGHT_CLUSTER = 'cluster';
    public const RIGHT_PROFILE = 'profile';
    public const RIGHT_PERSONALDATA = 'personaldata';
    public const RIGHT_DIRECTCALL = 'directcall';
    public const RIGHT_STATISTIC = 'statistic';
    public const RIGHT_GHOSTWORKSTATION = 'ghostworkingstations';
    public const RIGHT_SMS = 'sms';
    public const RIGHT_MAIL = 'mail';
    public const RIGHT_APPOINTMENT = 'appointment';
    public const RIGHT_PICKUP = 'pickup';
    public const RIGHT_EMERGENCY = 'emergency';
    public const RIGHT_AVAILABILITY = 'availability';
    public const RIGHT_REMOTECALL = 'remotecall';
    public const RIGHT_LOGIN = 'basic';

    public static $schema = "permission.json";

    public function getDefaults(): array
    {
        return [
            'remotecall' => 0
        ];
    }

    public function hasRight($right): bool
    {
        return 1 == $this->toProperty()->$right->get();
    }

    public function withAccessRights(array $accessRights): Permission
    {
        $entity = new self();
        $rightsList = [];
        foreach ($accessRights as $accessRight) {
            if (isset($accessRight['name']) && isset($accessRight['assigned'])) {
                $rightsList[$accessRight['name']] = (int)$accessRight['assigned'];
            }
        }
        $entity->addData($rightsList);
        return $entity;
    }
}
